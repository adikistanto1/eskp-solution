<?php

class MY_Loader extends CI_Loader{
    
    public function append($text, $return = false) {
        $this->output->append_output($text);
        if ($return)
            return $text;
    }
    
    public function viewAdminDashboard($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('admin/dashboard/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('admin/dashboard/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

    public function viewAdminSkp($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('admin/skp/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('admin/skp/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewAdminLaporan($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('admin/laporan/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('admin/laporan/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

    public function viewAdminProfil($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('admin/profil/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('admin/profil/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

    public function viewPNSDashboard($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('pns/dashboard/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('pns/dashboard/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

    public function viewPNSSkp($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('pns/skp/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('pns/skp/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewPNSLaporan($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('pns/laporan/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('pns/laporan/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

    public function viewPNSProfil($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('pns/profil/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('pns/profil/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }

}

