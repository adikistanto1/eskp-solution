<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class M_Skp extends CI_Model {
	
    public function simpan_baru_draft()
    {
       
        $data_create = array(
            'tgl_awal_skp'          => date('Y-m-d',strtotime($this->input->post('tgl_awal'))),
            'tgl_akhir_skp'         => date('Y-m-d',strtotime($this->input->post('tgl_akhir'))),
            
            'nip_pegawai'           => $this->input->post('nip-pegawai'),
            'golongan_pegawai'      => $this->input->post('pangkat-pegawai'),
            'jabatan_pegawai'       => $this->input->post('jabatan-pegawai'),
            'opd_pegawai'           => $this->input->post('opd-pegawai'),

            'nip_penilai'           => $this->input->post('nip-penilai'),
            'golongan_penilai'      => $this->input->post('pangkat-penilai'),
            'jabatan_penilai'       => $this->input->post('jabatan-penilai'),
            'opd_penilai'           => $this->input->post('opd-penilai'),

            'nip_atasan_penilai'           => $this->input->post('nip-atasan-penilai'),
            'golongan_atasan_penilai'      => $this->input->post('pangkat-atasan-penilai'),
            'jabatan_atasan_penilai'       => $this->input->post('jabatan-atasan-penilai'),
            'opd_atasan_penilai'           => $this->input->post('opd-atasan-penilai'),
            
            'status_skp_pegawai'   => 0,
            'create_date_skp'   => date("Y-m-d h:i:s")
        );

        $this->db->insert('tb_skp_pegawai', $data_create);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function simpan_edit_draft()
    {
       
        $data_create = array(
            'tgl_awal_skp'          => date('Y-m-d',strtotime($this->input->post('tgl_awal'))),
            'tgl_akhir_skp'         => date('Y-m-d',strtotime($this->input->post('tgl_akhir'))),
            
            'nip_pegawai'           => trim($this->input->post('nip-pegawai')),
            'golongan_pegawai'      => $this->input->post('pangkat-pegawai'),
            'jabatan_pegawai'       => $this->input->post('jabatan-pegawai'),
            'opd_pegawai'           => $this->input->post('opd-pegawai'),

            'nip_penilai'           => trim($this->input->post('nip-penilai')),
            'golongan_penilai'      => $this->input->post('pangkat-penilai'),
            'jabatan_penilai'       => $this->input->post('jabatan-penilai'),
            'opd_penilai'           => $this->input->post('opd-penilai'),

            'nip_atasan_penilai'           => trim($this->input->post('nip-atasan-penilai')),
            'golongan_atasan_penilai'      => $this->input->post('pangkat-atasan-penilai'),
            'jabatan_atasan_penilai'       => $this->input->post('jabatan-atasan-penilai'),
            'opd_atasan_penilai'           => $this->input->post('opd-atasan-penilai'),
                     
        );

        $id_skp = $this->input->post('id-skp');
        $this->db->where('id_skp', $id_skp);       
        $this->db->update('tb_skp_pegawai', $data_create);
    }

    public function simpan_kegiatan()
    {
        $id_skp_kegiatan = $this->input->post('id-skp-kegiatan');

        $data_create = array(    
            'id_skp_pegawai'            => $this->input->post('id-skp-pegawai'),
            'kegiatan_skp'              => $this->input->post('butir-kegiatan'),
            'total_angka_kredit'        => $this->input->post('total-angka-kredit'),
            'kuantitas_kegiatan'        => $this->input->post('kuantitas-kegiatan'),
            'output_kegiatan'           => $this->input->post('output-kegiatan'),
            'mutu_kegiatan'             => $this->input->post('mutu-kegiatan'),
            'bulan_kegiatan'            => $this->input->post('bulan-kegiatan'),
            'biaya_kegiatan'            => $this->input->post('biaya-kegiatan'),

            'create_date_kegiatan'   => date("Y-m-d h:i:s")
        );

        if($id_skp_kegiatan==0){
            $this->db->insert('tb_skp_kegiatan', $data_create);
        }else{
            $this->db->where('id_skp_kegiatan', $id_skp_kegiatan);       
            $this->db->update('tb_skp_kegiatan', $data_create);
        }
               
    }

    public function simpan_capaian()
    {
        $id_skp_capaian = $this->input->post('id-skp-capaian');

        $data_create = array(    
            'id_skp_pegawai'                => $this->input->post('id-skp-pegawai'),
            'id_skp_kegiatan'               => $this->input->post('id-skp-kegiatan'),           
            'total_angka_kredit_capaian'    => $this->input->post('capaian-total-angka-kredit'),
            'kuantitas_capaian'            => $this->input->post('capaian-kuantitas-kegiatan'),           
            'mutu_capaian'                 => $this->input->post('capaian-mutu-kegiatan'),
            'bulan_capaian'                => $this->input->post('capaian-bulan-kegiatan'),
            'biaya_capaian'                => $this->input->post('capaian-biaya-kegiatan'),

            'create_date_capaian'   => date("Y-m-d h:i:s")
        );

        if($id_skp_capaian==0){
            $this->db->insert('tb_skp_capaian', $data_create);
        }else{
            $this->db->where('id_skp_capaian', $id_skp_capaian);       
            $this->db->update('tb_skp_capaian', $data_create);
        }
               
    }

    public function simpan_penilaian()
    {
        
        $data_create = array(    
            'id_skp_pegawai'    => $this->input->post('id-skp'),
            'pelayanan'         => $this->input->post('pelayanan'),
            'integritas'        => $this->input->post('integritas'),
            'komitmen'          => $this->input->post('komitmen'),
            'disiplin'          => $this->input->post('disiplin'),
            'kerjasama'         => $this->input->post('kerjasama'),
            'kepemimpinan'      => $this->input->post('kepemimpinan'),            

            'create_date_perilaku'   => date("Y-m-d h:i:s")
        );

        $this->db->insert('tb_skp_perilaku', $data_create);
               
    }
	

    public function get_draft_all()
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai',0);
            $this->db->where('is_delete_skp',0);           
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_draft_all_by_nip($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai',0);
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_riwayat_all_by_nip($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai >',0);
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_skp_all_by_nip($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');           
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_selesai_by_nip($nip)
    {
            $this->db->select('a.*,b.*,c.*,d.*');
            $this->db->from('tb_skp_pegawai a');   
            $this->db->join('tb_skp_kegiatan b', 'a.id_skp=b.id_skp_pegawai','inner');
            $this->db->join('tb_skp_capaian c', 'b.id_skp_kegiatan=c.id_skp_kegiatan','inner');
            $this->db->join('tb_skp_perilaku d', 'a.id_skp=d.id_skp_pegawai','inner');   
            $this->db->where('a.is_delete_skp',0);
            $this->db->where('a.status_skp_pegawai',4);
            $this->db->where('a.nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_penilai_selesai_by_nip($nip)
    {
            $this->db->select('a.*,b.nama_pegawai as nama_penilai,b.gelar_depan as gelard_penilai,
                            b.gelar_belakang as gelarb_penilai,c.nama_pegawai as nama_apenilai,c.gelar_depan as gelard_apenilai,
                            c.gelar_belakang as gelarb_apenilai');
            $this->db->from('tb_skp_pegawai a');   
            $this->db->join('tb_pegawai b', 'a.nip_penilai=b.nip_baru','inner');  
            $this->db->join('tb_pegawai c', 'a.nip_atasan_penilai=c.nip_baru','inner');          
            $this->db->where('a.is_delete_skp',0);
            $this->db->where('a.status_skp_pegawai',4);
            $this->db->where('a.nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_id_selesai_by_nip($nip)
    {
            $this->db->select('a.*');
            $this->db->from('tb_skp_pegawai a');             
            $this->db->where('a.is_delete_skp',0);
            $this->db->where('a.status_skp_pegawai',4);
            $this->db->where('a.nip_pegawai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_draft_by_id($id_skp)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai',0);
            $this->db->where('is_delete_skp',0);
            $this->db->where('id_skp',$id_skp);
            $query = $this->db->get();
            return $query->row_array();
    }

    public function get_revisi_by_id($id_skp)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai',2);
            $this->db->where('is_delete_skp',0);
            $this->db->where('id_skp',$id_skp);
            $query = $this->db->get();
            return $query->row_array();
    }

    public function get_skp_by_id($id_skp)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');      
            $this->db->where('is_delete_skp',0);
            $this->db->where('id_skp',$id_skp);
            $query = $this->db->get();
            return $query->row_array();
    }

    public function get_skp_perilaku_by_id($id_skp)
    {
            $this->db->select('a.*,b.*');
            $this->db->from('tb_skp_pegawai a');
            $this->db->join('tb_skp_perilaku b', 'a.id_skp = b.id_skp_pegawai', 'left');       
            $this->db->where('is_delete_skp',0);
            $this->db->where('id_skp',$id_skp);
            $query = $this->db->get();
            return $query->row_array();
    }


    public function get_list_skp_to_verif($nip)
    {
            $this->db->select('a.*, b.gelar_depan,b.nama_pegawai,b.gelar_belakang');
            $this->db->from('tb_skp_pegawai a');
            $this->db->join('tb_pegawai b', 'a.nip_pegawai = b.nip_baru', 'inner');             
            $this->db->where('is_delete_skp',0);
            $this->db->where('status_skp_pegawai >',0);
            $this->db->where('nip_penilai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_list_capaian_to_verif($nip)
    {
            $this->db->select('a.*, b.gelar_depan,b.nama_pegawai,b.gelar_belakang');
            $this->db->from('tb_skp_pegawai a');
            $this->db->join('tb_pegawai b', 'a.nip_pegawai = b.nip_baru', 'inner');             
            $this->db->where('is_delete_skp',0);
            $this->db->where('status_skp_pegawai >',2);
            $this->db->where('status_skp_capaian >=',1);
            $this->db->where('nip_penilai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_list_penilaian_to_verif($nip)
    {
            $this->db->select('a.*, b.gelar_depan,b.nama_pegawai,b.gelar_belakang');
            $this->db->from('tb_skp_pegawai a');
            $this->db->join('tb_pegawai b', 'a.nip_pegawai = b.nip_baru', 'inner');             
            $this->db->where('is_delete_skp',0);
            $this->db->where('status_skp_pegawai >',2);
            $this->db->where('status_skp_perilaku >=',1);
            $this->db->where('nip_penilai',$nip);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function cek_skp_aktif($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $this->db->where('status_skp_pegawai',3);
            $query = $this->db->get();
            if($query->num_rows()>0){
				return 1;
			}else{
				return 0;
			}
    }

    public function cek_skp_terkirim($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $this->db->where('status_skp_pegawai',1);
            $query = $this->db->get();
            if($query->num_rows()>0){
				return 1;
			}else{
				return 0;
			}
    }

    public function cek_skp_revisi($nip,$id_skp)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $this->db->where('id_skp !=',$id_skp);
            $this->db->where('status_skp_pegawai',2);
            $query = $this->db->get();
            if($query->num_rows()>0){
				return 1;
			}else{
				return 0;
			}
    }

    

    public function get_skp_aktif($nip)
    {
            $this->db->select('a.*,b.*');
            $this->db->from('tb_skp_pegawai a');
            $this->db->join('tb_skp_perilaku b', 'a.id_skp = b.id_skp_pegawai', 'left');
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);
            $this->db->where('status_skp_pegawai ',3);
            $query = $this->db->get();
            return $query->row_array();
    }

    public function get_satuan_kegiatan_kecil()
    {
            $this->db->select('a.*');
            $this->db->from('tb_skp_satuan_kecil a');          
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_skp_kegiatan_by_id($id)
    {
            $this->db->select('a.*,b.*');
            $this->db->from('tb_skp_kegiatan a');
            $this->db->join('tb_skp_satuan_kecil b', 'a.output_kegiatan = b.id_skp_satuan_kecil', 'inner');
            $this->db->where('a.id_skp_pegawai',$id);
            $this->db->where('a.is_delete_kegiatan',0);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_skp_kegiatan_capaian_by_id($id)
    {
            $this->db->select('a.*,b.*,c.*,a.id_skp_kegiatan as id_kegiatan');
            $this->db->from('tb_skp_kegiatan a');
            $this->db->join('tb_skp_capaian b', 'a.id_skp_kegiatan = b.id_skp_kegiatan', 'left'); 
            $this->db->join('tb_skp_satuan_kecil c', 'a.output_kegiatan = c.id_skp_satuan_kecil', 'inner');
            $this->db->where('a.id_skp_pegawai',$id);
            $this->db->where('a.is_delete_kegiatan',0);
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_skp_penilaian_by_id($id)
    {
            $this->db->select('a.*,c.*');
            $this->db->from('tb_skp_pegawai a');           
            $this->db->join('tb_skp_perilaku c', 'a.id_skp = c.id_skp_pegawai', 'left');
            $this->db->where('a.id_skp',$id);
            $this->db->where('a.is_delete_skp',0);
            $query = $this->db->get();
            return $query->row_array();
    }

    

    public function hapus_draft($id){
        $data = array(        
            'is_delete_skp' => 1,
            'delete_date_skp' => date("Y-m-d h:i:s")
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function kirim_verifikasi_skp($id){
        $data = array(        
            'status_skp_pegawai' => 1           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function kirim_capaian_skp($id){
        $data = array(        
            'status_skp_capaian' => 1           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function kirim_perilaku_skp($id){
        $data = array(        
            'status_skp_perilaku' => 1           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function setuju_skp($id){
        $data = array(        
            'status_skp_pegawai' => 3           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function batalkan_skp($id){
        $data = array(        
            'status_skp_pegawai' => 5           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function selesai_skp($id){
        $data = array(        
            'status_skp_pegawai' => 4           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function setuju_penilaian(){
        $data = array(        
            'status_skp_perilaku' => 2           
        );
        $this->db->where('id_skp', $this->input->post('id-skp'));
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function revisi_skp($id){
        $data = array(        
            'status_skp_pegawai' => 2,
            'revisi_skp_pegawai' => $this->input->post('catatan-revisi')           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function setuju_capaian($id){
        $data = array(        
            'status_skp_capaian' => 3           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function revisi_capaian($id){
        $data = array(        
            'status_skp_capaian' => 2,
            'revisi_skp_capaian' => $this->input->post('catatan-revisi')           
        );
        $this->db->where('id_skp', $id);
        $this->db->update('tb_skp_pegawai', $data);
    }

    public function hapus_kegiatan($id){
        $data = array(        
            'is_delete_kegiatan' => 1,
            'delete_date_kegiatan' => date("Y-m-d h:i:s")
        );
        $this->db->where('id_skp_kegiatan', $id);
        $this->db->update('tb_skp_kegiatan', $data);
    }

    public function cari_laporan_skp($nip)
    {
            $status = $this->input->post('status');
            $tahun  = $this->input->post('tahun');
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');      
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_pegawai',$nip);     
            $this->db->where("DATE_FORMAT(tgl_awal_skp,'%Y')", $tahun);      
            if($status!='6'){
                $this->db->where('status_skp_pegawai',$status);
            }
            $query = $this->db->get();
            return $query->result_array();
    }


    
}