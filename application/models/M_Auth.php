<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Auth extends CI_Model {
   
	public function check_user($username,$password)
	{
		$query = $this->db->get_where('tb_admin_skp', array('username_admin_skp' => $username, 'password_admin_skp' => $password,'is_delete'=>0));
		return $query->row();
	}
	
	public function get_user($username)
	{
		$query = $this->db->get_where('tb_admin_skp', array('username_admin_skp' =>$username,'is_delete'=>0));
		return $query->row_array();
	}

	public function cek_status_verifikator($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_skp_pegawai');
            $this->db->where('status_skp_pegawai!=',0);
            $this->db->where('is_delete_skp',0);
            $this->db->where('nip_penilai',$nip);
            $query = $this->db->get();
			if($query->num_rows()>0){
				return 1;
			}else{
				return 0;
			}
            
    }
}