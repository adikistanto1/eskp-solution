<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class M_Pegawai extends CI_Model {
	
   
	public function get_pegawai_all()
    {
            $this->db->select('gelar_depan, gelar_belakang, nama_pegawai, nip_baru');
            $this->db->from('tb_pegawai');
            $query = $this->db->get();
            return $query->result_array();
    }

	public function get_pegawai_by_nip($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_pegawai');
            $this->db->join('tb_opd', 'tb_opd.id_opd = tb_pegawai.id_opd', 'inner');
            $this->db->where('tb_pegawai.nip_baru', $nip);
            //$this->db->where('tb_opd.status_opd', 'aktif');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_pegawai($nip)
    {
            $this->db->select('*');
            $this->db->from('tb_pegawai');
            $this->db->join('tb_opd', 'tb_opd.id_opd = tb_pegawai.id_opd', 'inner');
            $this->db->where('tb_pegawai.nip_baru', $nip);           
            $query = $this->db->get();
            return $query->row_array();
    }

    public function ubah_password($nip)
    {
       
        $data_create = array(    
            'password_admin_skp'   => sha1($this->input->post('password-baru'))           
        );

        $this->db->where('username_admin_skp', $nip);       
        $this->db->update('tb_admin_skp', $data_create);
               
    }
}