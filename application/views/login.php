<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - eSKP Solution</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/app.css">


    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/pages/auth.css">

    
    <script src="<?php echo base_url();  ?>assets/vendors/sweetalert2/sweetalert2.all.min.js"></script>
</head>

<body>
    <div id="auth">

        <div class="row h-100">
            <div class="col-lg-5 col-12">
                <div id="auth-left">
                    <div class="auth-logo mb-4">
                        <img style="width:300px;height:auto" src="<?= base_url(); ?>/assets/images/logo/logo.png" alt="Logo" srcset="">
                    </div>
                    <!-- <h1 class="auth-title">Log in.</h1> -->
                    <p class="auth-subtitle mb-5">Gunakan username dan password yang sudah terdaftar.</p>

                    <form action="<?php echo base_url(); ?>auth/login" method="post"> 
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input type="text" class="form-control form-control-xl" placeholder="Username" name="username">
                            <div class="form-control-icon">
                                <i class="bi bi-person"></i>
                            </div>
                        </div>
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input id="password" type="password" class="form-control form-control-xl" placeholder="Password" name="password">
                            <div class="form-control-icon">
                                <i class="bi bi-shield-lock"></i>
                            </div>
                        </div>
                        <div class="form-check form-check-lg d-flex align-items-end">
                            <input class="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault" onclick="showPassword()">
                            <label class="form-check-label text-gray-600" for="flexCheckDefault">
                               Show Password
                            </label>
                        </div>
                        <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Log in</button>
                    </form>
                    
                </div>
            </div>
            <?php
            
            if (isset($error)) {
                echo    '<script type="text/javascript">
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: "Username atau password salah!"
                        })
                        </script>';
            }
        
        ?> 
            <div class="col-lg-7 d-none d-lg-block">
                <div id="auth-right">
                    <div id="carouselExampleFade" class="carousel slide carousel-fade pt-5"
                        data-bs-ride="carouselfade">
                        <ol class="carousel-indicators">
                            <li data-bs-target="#carouselExampleFade" data-bs-slide-to="0"
                                class="active"></li>
                            <li data-bs-target="#carouselExampleFade" data-bs-slide-to="1"></li>
                          
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo base_url();  ?>assets/images/samples/pekalongan.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                           
                            <div class="carousel-item">
                                <img src="<?php echo base_url();  ?>assets/images/samples/pekalongan1.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleFade" role="button"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleFade" role="button"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>   
    
<script src="<?php echo base_url();  ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url();  ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();  ?>assets/js/main.js"></script>
<script>
    function showPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>   
    
</body>
</html>