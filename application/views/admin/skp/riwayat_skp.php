        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Riwayat SKP</h3>
                            <p class="text-subtitle text-muted">                               
                               Berisi daftar draft SKP yang belum selesai dibuat</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>admin/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Buat SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Kegiatan SKP</h4>
                                <a href="#" class="btn btn-primary rounded-pill mt-2" data-bs-toggle="modal" data-bs-target="#primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Tambah kegiatan</a>                                
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th>Waktu</th>
                                            <th>Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Graiden</td>                                          
                                            <td>Graiden</td>
                                            <td>Graiden</td>
                                            <td>vehicula.aliquet@semconsequat.co.uk</td>
                                            <td>076 4820 8838</td>
                                            <td>Offenburg</td>
                                            <td>
                                                <span class="badge bg-success">Active</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>               
            <div class="buttons">
               
                <a href="<?= base_url()?>admin/skp/buatbaru" class="btn btn-info rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Sebelumnya</a>
                <a href="#" class="btn btn-warning rounded-pill"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan sebagai draft</a>
                <a href="#" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Selanjutnya kirim ke pejabat penilai</a>
            </div>
        </div>       

        <!--primary theme Modal -->
        <div class="modal fade text-left " id="primary" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel160"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">
                            Tambah Kegiatan SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">                            
                            <div class="col-12">
                                <p>Masukkan kegiatan SKP beserta detailnya kegiatannya.
                                </p>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="roundText">Butir Kegiatan</label>
                                    <textarea type="text"  class="form-control round"
                                        placeholder="Input butir kegiatan" required></textarea>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="roundText">Total Angka Kredit</label>
                                    <input type="number"  class="form-control round"
                                        placeholder="Input Total AK" min="0" value="0.00" step=".01" required>                                   
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Kuantitas/Output</label>
                                <fieldset>
                                    <div class="input-group">                                                                      
                                        <input type="number" aria-label="First name" class="form-control"
                                            placeholder="Kuantitas" min="0" value="0" step="1">
                                        <select class="form-control">
                                            <option>Pilih</option>
                                            <option>Kegiatan</option>
                                            <option>Laporan</option>
                                            <option>Dokumen</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Kualitas Mutu</label>
                               
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" placeholder="Input"                                      
                                    min="0" value="100" step="1">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Waktu Realisasi</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" placeholder="Input"
                                    min="0" value="0" step="1">
                                    <span class="input-group-text" id="basic-addon2">Bulan</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Biaya</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input type="dec" class="form-control" placeholder="Input"
                                        aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            
                        </div>                                              
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary rounded-pill"
                            data-bs-dismiss="modal">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Batal</span>
                        </button>
                        <button type="button" class="btn btn-primary ml-1 rounded-pill"
                            data-bs-dismiss="modal">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
        
    