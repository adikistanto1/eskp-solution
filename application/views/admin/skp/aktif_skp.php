        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>SKP Aktif</h3>
                            <p class="text-subtitle text-muted">
                               Merupakan SKP yang sedang berlaku, SKP yang akan dinilai</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>admin/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Buat SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input type="text"  class="form-control round"
                                                placeholder="Input Tanggal" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input type="text" class="form-control square"
                                                placeholder="Input Tanggal" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data identitas pegawai yang dinilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="roundText">NIP</label>
                                            <input type="text"  class="form-control round"
                                                placeholder="Input NIP">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Nama</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Jabatan">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <input type="text" id="squareText" class="form-control square"
                                                placeholder="Input OPD">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data pejabat penilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="roundText">NIP</label>
                                            <input type="text"  class="form-control round"
                                                placeholder="Input NIP">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Nama</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Jabatan">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <input type="text" id="squareText" class="form-control square"
                                                placeholder="Input OPD">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Atasan Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data atasan pejabat penilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="roundText">NIP</label>
                                            <input type="text"  class="form-control round"
                                                placeholder="Input NIP">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Nama</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <input type="text"  class="form-control square"
                                                placeholder="Input Jabatan">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <input type="text" id="squareText" class="form-control square"
                                                placeholder="Input OPD">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Capaian Kegiatan&nbsp;&nbsp;<span class="badge bg-warning">Belum diisi</span></h4>
                            </div>

                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th>Waktu</th>
                                            <th>Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Graiden</td>                                          
                                            <td>Graiden</td>
                                            <td>Graiden</td>
                                            <td>vehicula.aliquet@semconsequat.co.uk</td>
                                            <td>076 4820 8838</td>
                                            <td>Offenburg</td>
                                            <td>
                                                <span class="badge bg-success">Active</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            <a href="<?= base_url()?>admin/skp/inputkegiatan" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Input Capaian Kinerja</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Perilaku Kerja&nbsp;&nbsp;<span class="badge bg-warning">Belum dinilai</span></h4>
                            </div>

                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th>Waktu</th>
                                            <th>Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Graiden</td>                                          
                                            <td>Graiden</td>
                                            <td>Graiden</td>
                                            <td>vehicula.aliquet@semconsequat.co.uk</td>
                                            <td>076 4820 8838</td>
                                            <td>Offenburg</td>
                                            <td>
                                                <span class="badge bg-success">Active</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            <a href="<?= base_url()?>admin/skp/inputkegiatan" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Input Capaian Kinerja</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- <div class="buttons">
                 <a href="#" class="btn btn-danger rounded-pill"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;Batal</a>
                <a href="#" class="btn btn-warning rounded-pill"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan sebagai draft</a>
                <a href="<?= base_url()?>admin/skp/inputkegiatan" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Input Capaian Kinerja</a>
            </div> -->
        </div>
        
    