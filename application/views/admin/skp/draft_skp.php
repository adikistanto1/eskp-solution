        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Draft SKP</h3>
                            <p class="text-subtitle text-muted">                               
                               Berisi daftar draft SKP yang belum selesai dibuat</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>admin/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Draft SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">                           
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Periode SKP</th>
                                            <th>Tahun</th>
                                            <th>Waktu Pembuatan</th>
                                            <th>Aksi</th>
                                                                                      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($draft as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>                                          
                                            <td><?= date( "d M Y", strtotime($item['tgl_awal_skp']))." - ".date( "d M Y", strtotime($item['tgl_akhir_skp']));?></td>
                                            <td><?= date( "Y", strtotime($item['tgl_awal_skp']))?></td>
                                            <td><?= date( "d M Y H:i:s", strtotime($item['create_date_skp']))?></td>
                                            <td>
                                                <button id="delete-button" data-bs-toggle="modal" data-id="<?=$item['id_skp']?>" data-bs-target="#danger" class="btn icon btn-danger rounded-pill" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Hapus">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                                <button id="edit-button" class="btn icon btn-success rounded-pill"
                                                data-bs-toggle="modal" data-id="<?=$item['id_skp']?>" data-bs-target="#edit"
                                                data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Edit">
                                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                                </button>
                                            </td>                                           
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                           
        </div>       
    </div>
    <?php
        $message = $this->session->flashdata('message');
        if (isset($message)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'.$message.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        } 
    ?>
    <!--Danger theme Modal -->
    <div class="modal fade text-left" id="danger" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title white" id="myModalLabel120">
                        Konfirmasi Hapus
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    Yakin akan menghapus data ini?
                    <form action="<?php echo base_url(); ?>admin/skp/hapusdraft" method="post" id="form-hapus-draft">
                        <input type="hidden" name="id-skp" id="id-skp" value=""/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Tidak</span>
                    </button>
                    <button onclick="submitForm()" type="button" class="btn btn-danger ml-1"
                        data-bs-dismiss="modal">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Iya</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="edit" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title white" id="myModalLabel120">
                        Konfirmasi Edit
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    Yakin akan ubah data ini?
                    <form action="<?php echo base_url(); ?>admin/skp/editdraft" method="post" id="form-edit-draft">
                        <input type="hidden" name="id-skp" id="id-skp" value=""/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Tidak</span>
                    </button>
                    <button onclick="submitEditForm()" type="button" class="btn btn-success ml-1"
                        data-bs-dismiss="modal">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Iya</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);    

    $(document).on("click", "#delete-button", function () {
        var id_skp = $(this).data('id');
        $(".modal-body #id-skp").val( id_skp );
    }); 

    $(document).on("click", "#edit-button", function () {
        var id_skp = $(this).data('id');
        $(".modal-body #id-skp").val( id_skp );
    }); 

    function submitForm() {       
        document.getElementById("form-hapus-draft").submit();
    }

    function submitEditForm() {       
        document.getElementById("form-edit-draft").submit();
    }
</script>
        
    