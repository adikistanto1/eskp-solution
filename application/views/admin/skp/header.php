<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Mazer Admin Dashboard</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/simple-datatables/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/iconly/bold.css">

    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/app.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/choices.js/choices.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/toastify/toastify.css">
    
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.svg" type="image/x-icon">

    <script src="<?= base_url(); ?>assets/vendors/jquery/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/simple-datatables/simple-datatables.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/toastify/toastify.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/sweetalert2/sweetalert2.all.min.js"></script>
</head>

<body>
    <div id="app">    
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="index.html"><img src="<?= base_url(); ?>/assets/images/logo/logo.png" alt="Logo" srcset=""></a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li class="sidebar-item ">
                            <a href="<?= base_url()?>admin/dashboard/index" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        

                        <li class="sidebar-item  has-sub active">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-stack"></i>
                                <span>SKP</span>
                            </a>
                            <ul id="submenu-skp" class="submenu active">
                                <li class="submenu-item <?php if($menu=='skp-baru'){echo 'active';}?>">
                                    <a href="<?= base_url()?>admin/skp/baru">Buat SKP</a>
                                </li>
                                <li class="submenu-item <?php if($menu=='skp-draft'){echo 'active';}?>">
                                    <a href="<?= base_url()?>admin/skp/draft">Draft SKP</a>
                                </li>
                                <li class="submenu-item <?php if($menu=='skp-aktif'){echo 'active';}?>">
                                    <a href="<?= base_url()?>admin/skp/aktif">SKP Aktif</a>
                                </li>
                                <li class="submenu-item <?php if($menu=='skp-riwayat'){echo 'active';}?>">
                                    <a href="<?= base_url()?>admin/skp/riwayat">Riwayat SKP</a>
                                </li>                                
                            </ul>
                        </li>

                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-collection-fill"></i>
                                <span>Laporan</span>
                            </a>
                            <ul class="submenu ">
                                <li class="submenu-item ">
                                    <a href="<?= base_url()?>admin/laporan/index">Cetak Laporan</a>
                                </li>
                                
                            </ul>
                        </li>

                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-grid-1x2-fill"></i>
                                <span>User</span>
                            </a>
                            <ul class="submenu ">
                                <li class="submenu-item ">
                                    <a href="<?= base_url()?>admin/profil/index">Profil</a>
                                </li>
                                <li class="submenu-item ">
                                    <a href="#">Ubah Password</a>
                                </li> 
                                <li class="submenu-item ">
                                    <a href="<?= base_url()?>auth/logout">Logout</a>
                                </li>                                
                            </ul>
                        </li>
                    
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>       
        <div id="main" class='layout-navbar'>
    <header class='mb-0'>
            <nav class="navbar navbar-expand navbar-light ">
                <div class="container-fluid">
                    <a href="#" class="burger-btn d-block">
                        <i class="bi bi-justify fs-3"></i>
                    </a>

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown me-1">
                                <a class="nav-link active dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    <i class='bi bi-envelope bi-sub fs-4 text-gray-600'></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li>
                                        <h6 class="dropdown-header">Mail</h6>
                                    </li>
                                    <li><a class="dropdown-item" href="#">No new mail</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown me-3">
                                <a class="nav-link active dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    <i class='bi bi-bell bi-sub fs-4 text-gray-600'></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li>
                                        <h6 class="dropdown-header">Notifications</h6>
                                    </li>
                                    <li><a class="dropdown-item">No notification available</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="dropdown">
                            <a href="#" data-bs-toggle="dropdown" aria-expanded="false">
                                <div class="user-menu d-flex">
                                    <div class="user-name text-end me-3">
                                        <h6 class="mb-0 text-gray-600"><?= $nama;?></h6>
                                        <p class="mb-0 text-sm text-gray-600"><?= $role;?></p>
                                    </div>
                                    <div class="user-img d-flex align-items-center">
                                        <div class="avatar avatar-md">
                                            <img src="<?= base_url()?>assets/images/faces/1.jpg">
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                <li>
                                    <h6 class="dropdown-header">Hello, John!</h6>
                                </li>
                                <li><a class="dropdown-item" href="<?= base_url()?>admin/profil/index"><i class="icon-mid bi bi-person me-2"></i> My
                                        Profil</a></li>
                                <li><a class="dropdown-item" href="#"><i class="icon-mid bi bi-gear me-2"></i>
                                        Ubah Password</a></li>                               
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="<?= base_url()?>auth/logout"><i
                                            class="icon-mid bi bi-box-arrow-left me-2"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>       
<!-- <script>
// Add active class to the current button (highlight it)
var header = document.getElementById("submenu-skp");
var btns = header.getElementsByClassName("submenu-item");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("submenu-item active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
</script> -->