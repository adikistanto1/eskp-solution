        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Buat SKP Baru (2 dari 2 halaman)</h3>
                            <p class="text-subtitle text-muted">                               
                               Pada halaman ini berisi pengisian kegiatan-kegiatan SKP</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>admin/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Buat SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Kegiatan SKP</h4>
                                <button id="tambah-button" class="btn btn-primary rounded-pill mt-2" data-bs-toggle="modal" data-bs-target="#tambah-kegiatan"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Tambah kegiatan</button>                                
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th>Waktu</th>
                                            <th>Biaya</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($kegiatan as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>                                          
                                            <td><?= $item['kegiatan_skp'];?></td>
                                            <td><?= $item['total_angka_kredit'];?></td>
                                            <td><?= $item['kuantitas_kegiatan']." ".$item['output_kegiatan'];?></td>
                                            <td><?= $item['mutu_kegiatan']." %";?></td>
                                            <td><?= $item['bulan_kegiatan']." Bulan";?></td>
                                            <td><?= "Rp ".$item['biaya_kegiatan'];?></td>
                                            <td>
                                                <button id="delete-button" data-bs-toggle="modal" data-id="<?=$item['id_skp_kegiatan']?>" data-bs-target="#hapus" class="btn icon btn-danger rounded-pill" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Hapus">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>

                                                <button id="edit-button" class="btn icon btn-success rounded-pill"
                                                data-bs-toggle="modal" 
                                                data-bs-target="#tambah-kegiatan"
                                                data-id="<?=$item['id_skp_kegiatan']?>"
                                                data-kegiatan="<?=$item['kegiatan_skp']?>"
                                                data-kredit="<?=$item['total_angka_kredit']?>" 
                                                data-kuantitas="<?=$item['kuantitas_kegiatan']?>"
                                                data-output="<?=$item['output_kegiatan']?>"
                                                data-mutu="<?=$item['mutu_kegiatan']?>"
                                                data-bulan="<?=$item['bulan_kegiatan']?>"
                                                data-biaya="<?=$item['biaya_kegiatan']?>"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top" title="Edit">
                                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>               
            <div class="buttons">
                <form action="<?php echo base_url(); ?>admin/skp/editdraft" method="post" id="form-edit-draft">
                    <input type="hidden" name="id-skp" id="id-skp" value=""/>                   
                </form>
                <button type="button" onclick="submitEditDraftForm()" class="btn btn-info rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Sebelumnya</button>                
                <a href="#" class="btn btn-warning rounded-pill"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan sebagai draft</a>
                <a href="#" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Selanjutnya kirim ke pejabat penilai</a>
            </div>
        </div>       

        <!--Modal tambah kegiatan awal-->
        <div class="modal fade text-left " id="tambah-kegiatan" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel160"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl"
                role="document">
                <div class="modal-content">
                <form id="form-tambah-kegiatan" action="<?php echo base_url(); ?>admin/skp/simpankegiatan" method="post">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">
                            Tambah Kegiatan SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="id-skp-kegiatan" name="id-skp-kegiatan" value="0"/>
                        <div class="row">                            
                            <div class="col-12">
                                <p>Masukkan kegiatan SKP beserta detailnya kegiatannya.
                                </p>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="roundText">Butir Kegiatan</label>
                                    <textarea type="text" id="kegiatan" class="form-control round" name="butir-kegiatan"
                                        placeholder="Input butir kegiatan" ></textarea>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="roundText">Total Angka Kredit</label>
                                    <input type="number"  class="form-control round" id="kredit" name="total-angka-kredit"
                                        placeholder="Input Total AK" min="0" value="0.00" step=".01">                                   
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Kuantitas/Output</label>
                                <fieldset>
                                    <div class="input-group">                                                                      
                                        <input type="number" aria-label="First name" id="kuantitas" class="form-control" name="kuantitas-kegiatan"
                                            placeholder="Kuantitas" min="0" value="0" step="1">
                                        <select class="form-control" id="output" name="output-kegiatan">
                                            <option value="0">Pilih</option>
                                            <option value="1">Kegiatan</option>
                                            <option value="2">Laporan</option>
                                            <option value="3">Dokumen</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Kualitas Mutu</label>
                               
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" id="mutu" placeholder="Input" name="mutu-kegiatan"                                     
                                    min="0" value="100" max="100" step="1">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Waktu Realisasi</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" placeholder="Input" name="bulan-kegiatan"
                                    min="0" value="0" step="1" max="12" id="bulan">
                                    <span class="input-group-text" id="basic-addon2">Bulan</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Biaya</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input type="dec" min="0" value="0" class="form-control" placeholder="Input" name="biaya-kegiatan"
                                        aria-label="Username" aria-describedby="basic-addon1" id="biaya">
                                </div>
                            </div>
                            <p class="float-left">*Semua isian wajib diisi!</p>
                        </div>                                              
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id-skp-pegawai" value="<?= $id_skp_pegawai;?>"/>
                        
                        <button type="button"
                            class="btn btn-light-secondary rounded-pill"
                            data-bs-dismiss="modal">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Batal</span>
                        </button>
                        <button type="button" onclick="submitForm()"  class="btn btn-primary ml-1 rounded-pill">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan</span>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!--Modal tambah kegiatan akhir-->

        <!-- Modal hapus edit awal-->  
        <?php       
        if (isset($success)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'. $success.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        } 
    ?>     
        <div class="modal fade text-left" id="hapus" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel120"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Konfirmasi Hapus
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        Yakin akan menghapus data ini?
                        <form action="<?php echo base_url(); ?>admin/skp/hapuskegiatan" method="post" id="form-hapus-kegiatan">
                            <input type="hidden" name="id-skp-kegiatan" id="id-skp-kegiatan" value=""/>
                            <input type="hidden" name="id-skp-pegawai" value="<?= $id_skp_pegawai;?>"/>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Tidak</span>
                        </button>
                        <button onclick="submitHapusForm()" type="button" class="btn btn-danger ml-1"
                            data-bs-dismiss="modal">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Iya</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>        
        <!-- Modal hapus edit akhir-->
    </div>
<script>
function submitForm() {       

    var kegiatan        = document.getElementsByName('butir-kegiatan');
    var angka_kredit    = document.getElementsByName('total-angka-kredit');
    var kuantitas       = document.getElementsByName('kuantitas-kegiatan');
    var output          = document.getElementsByName('output-kegiatan');      
    var mutu            = document.getElementsByName('mutu-kegiatan');
    var bulan           = document.getElementsByName('bulan-kegiatan');
    var biaya           = document.getElementsByName('biaya-kegiatan'); 

    var isError = false;
  
    if(kegiatan[0].value===''){
        showErorValidate('Butir kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(angka_kredit[0].value===''){
        showErorValidate('Total angka kredit harus diisi!');
        var isError = true;
        return;
    }

    if(kuantitas[0].value===''){
        showErorValidate('Kuantitas kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(output[0].value===''){
        showErorValidate('Output kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(mutu[0].value===''){
        showErorValidate('Mutu kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(bulan[0].value===''){
        showErorValidate('Bulan kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(biaya[0].value===''){
        showErorValidate('Biaya kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(isError===false){
        document.getElementById("form-tambah-kegiatan").submit();
        return;
    }
}

    function showErorValidate(input){
        Swal.fire({
                icon: "error",
                title: "Oops...",
                text: input
            });
    }

    $(document).on("click", "#delete-button", function () {
        var id_skp = $(this).data('id');
        $(".modal-body #id-skp-kegiatan").val( id_skp );
    }); 

    $(document).on("click", "#edit-button", function () {
        var id          = $(this).data('id');
        var kegiatan    = $(this).data('kegiatan');
        var kredit      = $(this).data('kredit');
        var kuantitas   = $(this).data('kuantitas');
        var output      = $(this).data('output');
        var mutu        = $(this).data('mutu');
        var bulan       = $(this).data('bulan');
        var biaya       = $(this).data('biaya');
        $(".modal-body #id-skp-kegiatan").val( id );
        $(".modal-body #kegiatan").val( kegiatan );
        $(".modal-body #kredit").val( kredit );
        $(".modal-body #kuantitas").val( kuantitas );
        $(".modal-body #output").val( output );
        $(".modal-body #mutu").val( mutu );
        $(".modal-body #bulan").val( bulan );
        $(".modal-body #biaya").val( biaya );
    }); 

    $(document).on("click", "#tambah-button", function () {             
        $(".modal-body #kegiatan").val("");
        $(".modal-body #kredit").val( "0" );
        $(".modal-body #kuantitas").val( "0" );
        $(".modal-body #output").val( "0" );
        $(".modal-body #mutu").val( "100" );
        $(".modal-body #bulan").val( "0" );
        $(".modal-body #biaya").val( "0" );
    }); 

    function submitHapusForm() {       
        document.getElementById("form-hapus-kegiatan").submit();
    }

    function submitEditForm() {       
        document.getElementById("form-edit-kegiatan").submit();
    }
</script>
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);     
</script>
   
        
    