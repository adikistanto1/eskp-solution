<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    
</head>
<body>
   
<div class="container">
    <div class="row justify-content-center">
        <div style="width: 30rem;">
            <h2 class="mt-5" style="text-align: center;">SELAMAT DATANG DI E-SKP</h2>
            <p style="text-align: center;">Untuk dapat menggunakan aplikasi Anda harus melakukan login terlebih dahulu</p>
        </div>
        
        
   </div>
   <div class="row justify-content-center">             
        <div class="card mt-2" style="width: 25rem;">
            <img class="card-img-top" src="<?= base_url(); ?>assets/img/img-login.png" alt="Card image cap">
            <div class="card-body">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Masukkan NIP" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                    </div>
                    <input id="password" type="password" class="form-control" placeholder="Masukkan password" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                <div class="form-check float-right">
                    <input class="form-check-input" type="checkbox" onclick="myFunction()">
                    <label class="form-check-label" for="flexCheckDefault">
                        Tampilkan password
                    </label>
                </div>                
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    &nbsp;Masuk
                </button>
            </div>
            <div class="card-footer">
            <p class="font-weight-light" style="text-align: center;">&copy 2021 BKPPD Kota Pekalongan</p>
            </div>
        </div>
   </div>
</div>
  

<script src="<?= base_url(); ?>/assets/js/jquery-2.2.3.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>
<script>
function myFunction() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    }
</script>
</body>
</html>