<footer>
            <div class="footer clearfix mb-0 text-muted">
            <div class="float-start">
                    
                    </div>
                    <div class="float-end">
                    <p class="me-4">2021 &copy; BKPPD Kota Pekalongan</p>
                    </div>
            </div>
        </footer>
    </div>
</div>
 <script src="<?= base_url(); ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/fontawesome/all.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?= base_url(); ?>assets/js/main.js"></script>
</body>

</html>