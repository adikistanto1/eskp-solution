        

        <div id="main-content">
        <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-8 order-md-1 order-last">
                        <h3>LAPORAN</h3>
                        <p class="text-subtitle text-muted">
                            Halaman ini untuk melihat laporan pembuatan SKP.</p>
                    </div>
                    <div class="col-12 col-md-4 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Laporan</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Cari SKP</h4>
                            </div>

                            <form method="post" action="<?= base_url()?>pns/laporan/cari">
                                <div class="card-body">
                                    <div class="row">
                                    
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="roundText">Tahun SKP</label>                                                                                        
                                                <input autocomplete="off" type="text"  name="tahun" class="datepicker form-control square" required placeholder="Masukkan tahun"
                                                value=<?php if($tahun!=""){echo $tahun;}?>>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="squareText">Status SKP</label>
                                                <select name="status" class="form-control square" name="status" required>
                                                    <option value="">Pilih</option>
                                                    <option value="6" <?php if($status=="6"){echo 'selected';}?>>Semua</option>
                                                    <option value="0" <?php if($status=="0"){echo 'selected';}?>>Draft</option>                                                  
                                                    <option value="2" <?php if($status=="2"){echo 'selected';}?>>Revisi</option>
                                                    <option value="3" <?php if($status=="3"){echo 'selected';}?>>Aktif</option>
                                                    <option value="4" <?php if($status=="4"){echo 'selected';}?>>Selesai</option>
                                                    <option value="5" <?php if($status=="5"){echo 'selected';}?>>Dibatalkan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <br>
                                                <button type="submit"  class="btn btn-primary rounded-pill">
                                                    <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Cari
                                                </button>
                                            </div>
                                        </div>
                                        
                                        
                                        <p>*Jika ada tidak sesuai silahkan laporkan ke BKPPD agar data tersebut diperbarui.</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">                           
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Periode SKP</th>
                                            <th>Tahun</th>
                                            <th>Waktu Pembuatan</th>
                                            <th>Status</th>                                           
                                                                                      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($skp as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>                                          
                                            <td><?= date( "d M Y", strtotime($item['tgl_awal_skp']))." - ".date( "d M Y", strtotime($item['tgl_akhir_skp']));?></td>
                                            <td><?= date( "Y", strtotime($item['tgl_awal_skp']))?></td>
                                            <td><?= date( "d M Y H:i:s", strtotime($item['create_date_skp']))?></td>
                                            <td>
                                            <?php
                                                 if($item['status_skp_pegawai']==1){
                                                    echo '<span class="badge bg-primary">menunggu verifikasi</span>';
                                                 }else if($item['status_skp_pegawai']==3){
                                                    echo '<span class="badge bg-success">aktif</span>';
                                                 }else if($item['status_skp_pegawai']==2){
                                                    echo '<span class="badge bg-danger">revisi</span><br>Catatan:<br>'.$item['revisi_skp_pegawai'].'';
                                                 }else if($item['status_skp_pegawai']==4){
                                                    echo '<span class="badge bg-secondary">selesai</span>';
                                                 }else if($item['status_skp_pegawai']==5){
                                                    echo '<span class="badge bg-secondary">dibatalkan</span>';
                                                 }else if($item['status_skp_pegawai']==0){
                                                    echo '<span class="badge bg-warning">draft</span>';
                                                 }
                                                 
                                                 ?>
                                            </td>                                           
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> 
                
            </div>
        </div>
<script>
$(document).ready(function() {
   
     $('.datepicker').datepicker({
        format: "yyyy",
        viewMode: "years", 
        autoclose: true,
        minViewMode: "years"
    });
});

let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);
</script>
    