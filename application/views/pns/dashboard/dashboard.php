      
        <div id="main-content">
            <div class="page-heading">
                <h3>SKP Statistik</h3>
            </div>
            <div class="page-content">
                <section class="row">
                    <?php
                        $draft = 0;$menunggu = 0;$revisi = 0;$selesai = 0;$dibatalkan = 0;
                        foreach ($skp as $item){
                            if($item['status_skp_pegawai']=='0'){
                                $draft = $draft + 1;
                            }else if($item['status_skp_pegawai']=='1'){
                                $menunggu = $menunggu + 1;
                            }else if($item['status_skp_pegawai']=='2'){
                                $revisi = $revisi + 1;
                            }else if($item['status_skp_pegawai']=='4'){
                                $selesai = $selesai + 1;
                            }else if($item['status_skp_pegawai']=='5'){
                                $dibatalkan = $dibatalkan + 1;
                            }
                        }
                    ?>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body px-3 py-4-5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stats-icon purple">
                                                    
                                                    <i class="iconly-boldPaper"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h6 class="text-muted font-semibold">SKP Draft</h6>
                                                <h6 class="font-extrabold mb-0"><?= $draft;?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body px-3 py-4-5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stats-icon blue">
                                                    <i class="iconly-boldTime-Circle"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h6 class="text-muted font-semibold">Menunggu Verifikasi</h6>
                                                <h6 class="font-extrabold mb-0"><?= $menunggu;?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body px-3 py-4-5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stats-icon red">
                                                    <i class="iconly-boldMessage"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h6 class="text-muted font-semibold">Revisi</h6>
                                                <h6 class="font-extrabold mb-0"><?= $revisi;?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body px-3 py-4-5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stats-icon green">
                                                    <i class="iconly-boldBookmark"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h6 class="text-muted font-semibold">Selesai</h6>
                                                <h6 class="font-extrabold mb-0"><?= $selesai;?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body px-3 py-4-5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stats-icon black">
                                                    <i class="iconly-boldDanger"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h6 class="text-muted font-semibold">Dibatalkan</h6>
                                                <h6 class="font-extrabold mb-0"><?= $dibatalkan;?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Riwayat Penilaian</h4>
                                    </div>
                                    <div class="card-body">
                                        <!-- <div id="chart-profile-visit"></div> -->
                                        <div id="bar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Jumlah Kegiatan</h4>
                                    </div>
                                    <div class="card-body">
                                        <div id="batang"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-xl-8">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Riwayat Data Penilai</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-lg">
                                                <thead>
                                                    <tr>
                                                        <th>Periode</th>
                                                        <th>Penilai</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($penilai as $item):?>
                                                    <tr>
                                                        <td class="col-5">
                                                            <?= date( "d M", strtotime($item['tgl_awal_skp'])).' s/d '.date( "d M Y", strtotime($item['tgl_akhir_skp']));?>
                                                        </td>
                                                        <td class="col-auto">
                                                            <span class="badge bg-primary">PENILAI</span><br>
                                                            <?= $item['gelard_penilai'].' '.$item['nama_penilai'].', '.$item['gelarb_penilai']?><br><br>
                                                            <span class="badge bg-danger">ATASAN PENILAI</span><br>
                                                            <?= $item['gelard_apenilai'].' '.$item['nama_apenilai'].', '.$item['gelarb_apenilai']?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </section>
            </div>
        </div>

        <?php 
            $total_perilaku = 0;
            $rata_perilaku  = 0;
            $rata_perilaku_array  = array();
          
            foreach($id_skps as $id){

                foreach($list as $skp){
                    if($skp['id_skp']==$id['id_skp']){
                        $pel    = $skp['pelayanan'];
                        $int    = $skp['integritas'];
                        $kom    = $skp['komitmen'];
                        $dis    = $skp['disiplin'];
                        $ker    = $skp['kerjasama'];
                        $kep    = $skp['kepemimpinan'];

                        if($kep==0){
                            $total  = $pel + $int + $kom + $dis + $ker;
                            $rata_perilaku   = $total / 5;
                        }else{
                            $total  = $pel + $int + $kom + $dis + $ker + $kep;
                            $rata_perilaku   = $total / 6;
                        }
                        
                    
                        
                        

                    }
        
                }

                array_push($rata_perilaku_array,$rata_perilaku);
            }

              
            
            $jml_kegiatan_array  = array();
            $rata_capaian_array  = array();
            $periode_array  = array();           
            foreach($id_skps as $id){
                $nilai_capaian_total    = 0;
                $jml_kegiatan           = 0;
                $periode_skp = "";
                $total_nilai = 0;
                $kegiatan_dinilai = 0;
                foreach($list as $item){
                
                    if($item['id_skp']==$id['id_skp']){
                        // $kuantitas_kegiatan = $skp['kuantitas_kegiatan'];
                        // $kuantitas_capaian = $skp['kuantitas_capaian'];
                        // $nilai_kuantitas = ($kuantitas_capaian/$kuantitas_kegiatan)*100;
        
                        // $mutu_kegiatan = $skp['mutu_kegiatan'];
                        // $mutu_capaian = $skp['mutu_capaian'];
                        // $nilai_mutu = ($mutu_capaian/$mutu_kegiatan)*100;
        
                        // $nilai_capaian_kegiatan = ($nilai_kuantitas + $nilai_mutu)/2;
        
                        // $nilai_capaian_total = $nilai_capaian_total + $nilai_capaian_kegiatan;
                        // $jml_kegiatan        = $jml_kegiatan + 1;

                        $bulan_capaian = $item['bulan_capaian']=='' ? 0 : $item['bulan_capaian'];
                        $biaya_capaian = $item['biaya_capaian']=='' ? 0 : $item['biaya_capaian'];
                        $kuantitas_capaian = $item['kuantitas_capaian']=='' ? 0 : $item['kuantitas_capaian'];
                        $kualitas_capaian = $item['mutu_capaian']=='' ? 0 : $item['mutu_capaian'];

                        $persen_waktu = 100 - ($bulan_capaian/$item['bulan_kegiatan']*100);
                        $persen_biaya = $item['biaya_kegiatan']==0 ? 0 : 100 - ($biaya_capaian/$item['biaya_kegiatan']*100);
                        $kuantitas = $kuantitas_capaian/$item['kuantitas_kegiatan']*100;
                        $kualitas = $kualitas_capaian/$item['mutu_kegiatan']*100;

                        if($persen_waktu>24){
                            $waktu = 76 - ((((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100)-100);
                        }else{
                            $waktu = ((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100;
                        }

                        if(($biaya_capaian==0)||($item['biaya_kegiatan']==0)){
                            $biaya=0;
                        }else{
                            if($persen_biaya>24){
                                $biaya = 76 - ((((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100)-100);
                            }else{
                                $biaya = ((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100;
                            }
                        }

                        $perhitungan = $kuantitas + $kualitas + $waktu;
                        $perhitungan1 = number_format((float)$perhitungan, 2, '.', '');
                        $nilai       = $perhitungan1 / 3;
                        $nilai1       = number_format((float)$nilai, 2, '.', '');

                        if($item['kuantitas_kegiatan']>0){
                            $kegiatan_dinilai = $kegiatan_dinilai + 1;
                        }

                        $total_nilai = $total_nilai + $nilai1;

                        $periode_skp = '\''.date( "d M", strtotime($skp['tgl_awal_skp'])).' s/d '.date( "d M Y", strtotime($skp['tgl_akhir_skp'])).'\'';
                        
                    }
                }
                $nilai_capaian_akhir = number_format((float)$total_nilai/$kegiatan_dinilai, 2, '.', '');
                array_push($rata_capaian_array,$nilai_capaian_akhir);
                array_push($periode_array,$periode_skp);
                array_push($jml_kegiatan_array,$kegiatan_dinilai);
            }
            
            $prestasi_array  = array();
            $x = 0;            
            while($x < count($rata_capaian_array)) {
                $pr=0;
                $pr = number_format((float)(($rata_capaian_array[$x]*0.6)+($rata_perilaku_array[$x]*0.4)), 2, '.', '');
                array_push($prestasi_array,$pr);
                $x++;
            }
            //var_dump($selesai);
        ?>

        
        
<script>

    
var barOptions = {
    series: [
        {
        name: "Jumlah Kegiatan",
        data: [<?= implode(', ',$jml_kegiatan_array)?>],
        }
        
    ],
    chart: {
        type: "bar",
        height: 350,
    },
    plotOptions: {
        bar: {
        horizontal: false,
        columnWidth: "55%",
        endingShape: "rounded",
        },
    },
    dataLabels: {
        enabled: false,
    },
    stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
    },
    xaxis: {
        categories: [<?= implode(', ',$periode_array)?>],
    },
    yaxis: {
        title: {
        text: "Jumlah Kegiatan",
        },
    },
    fill: {
        opacity: 1,
    },
    tooltip: {
        y: {
        formatter: function(val) {
            return  val;
        },
        },
    },
    };

var areaOptions = {
   series: [
     {
       name: "Capaian Kerja",
       data: [<?= implode(', ',$rata_capaian_array)?>],
     },
     {
       name: "Perilaku Kerja",
       data: [<?= implode(', ',$rata_perilaku_array)?>],
     },
     {
       name: "Prestasi Kerja",
       data: [<?= implode(', ',$prestasi_array)?>],
     },
   ],
   chart: {
     height: 350,
     type: "area",
   },
   dataLabels: {
     enabled: false,
   },
   stroke: {
     curve: "smooth",
   },
   xaxis: {
     
     categories: [
        <?= implode(', ',$periode_array)?>
     ],
   },
   tooltip: {
     x: {
       format: "dd/MM/yy",
     },
   },
 };

    var bar = new ApexCharts(document.querySelector("#batang"), barOptions);
    var area = new ApexCharts(document.querySelector("#bar"), areaOptions);
    area.render();
    bar.render();
</script>