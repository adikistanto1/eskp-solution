        

        <div id="main-content">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-8 order-md-1 order-last">
                        <h3>PROFIL</h3>
                        <p class="text-subtitle text-muted">
                            Halaman ini berisi informasi profil pengguna.</p>
                    </div>
                    <div class="col-12 col-md-4 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profil</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                   
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                                                                        
                                            <input type="text"  class="form-control square"  readonly
                                                 required value="<?= $pegawai['gelar_depan'].' '.$pegawai['nama_pegawai'].' '.$pegawai['gelar_belakang']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square"  readonly
                                               value="<?= $pegawai['nip_baru']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label for="squareText">Tempat, Tanggal Lahir</label>
                                            <input type="text"  class="form-control square" readonly
                                            value="<?= $pegawai['tempat_lahir'].', '.$pegawai['tanggal_lahir']?>">
                                        </div>
                                    </div>   
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" value="<?= $pegawai['golongan_ruang']?>" readonly>
                                        </div>
                                    </div>                                                                     
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label for="squareText">Jenis Jabatan</label>
                                            <input type="text"  class="form-control square" value="<?= $pegawai['jenis_jabatan']?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" readonly><?= $pegawai['nama_jabatan']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" readonly><?= $pegawai['opd']?></textarea>
                                        </div>
                                    </div>
                                    <p>*Jika ada tidak sesuai silahkan laporkan ke BKPPD agar data tersebut diperbarui.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    