        

        <div id="main-content">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-8 order-md-1 order-last">
                        <h3>UBAH PASSWORD</h3>
                        <p class="text-subtitle text-muted">
                            Halaman ini berisi untuk merubah password pengguna.</p>
                    </div>
                    <div class="col-12 col-md-4 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Ubah Password</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Password</h4>
                            </div>
                            <form action="<?php echo base_url(); ?>pns/profil/ubahpassword" method="post"> 
                            <div class="card-body">
                                <div class="row">
                                   <div class="col-12">
                                        <p>Isi formulir berikut untuk merubah password
                                        </p>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="roundText">Password Lama*</label>                                                                                        
                                                <input type="password"  class="form-control square" name="password-lama" id="password-lama"
                                                    required placeholder="Masukkan password lama">
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class="form-group">
                                           <br>
                                                <input class="form-check-input me-2" type="checkbox"  onclick="showPasswordLama()">
                                                <label class="form-check-label text-gray-600" >
                                                    Tampilkan Password
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <br>
                                                <label for="roundText">Password Baru*</label>                                                                                        
                                                <input type="password"  class="form-control square" name="password-baru" id="password-baru"
                                                    required placeholder="Masukkan password baru">
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class="form-group">
                                           <br><br>
                                                <input class="form-check-input me-2" type="checkbox"  onclick="showPasswordBaru()">
                                                <label class="form-check-label text-gray-600" >
                                                    Tampilkan Password
                                                </label>
                                            </div>
                                        </div>
                                    </div>   
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <br>
                                                <label for="roundText">Konfirmasi Password Baru*</label>                                                                                        
                                                <input type="password"  class="form-control square" name="password-baru-konfirmasi" id="password-baru-konfirmasi"
                                                    required placeholder="Masukkan konfirmasi password baru">
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class="form-group">
                                           <br><br>
                                                <input class="form-check-input me-2" type="checkbox" onclick="showPasswordBaruKonfirmasi()">
                                                <label class="form-check-label text-gray-600" >
                                                    Tampilkan Password
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <p>*Isian wajib diisi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="buttons">     
                <input type="hidden" name="nip" value="<?= $nip;?>"  >                           
                <button type="submit" class="btn btn-success rounded-pill"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
            </div>
        </div>
        <?php       
            if (isset($success)) {
                echo    '<script type="text/javascript">
                Toastify({
                    text: "'. $success.'",
                    duration: 3000,
                    close:true,
                    gravity:"top",
                    position: "center",
                    backgroundColor: "#4fbe87",
                }).showToast();
                </script>';
            } 

            if (isset($error)) {
                echo    '<script type="text/javascript">
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: "'.$error.'"
                        })
                        </script>';
            }
        ?>
        
<script>
    function showPasswordLama() {
        var x = document.getElementById("password-lama");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

    function showPasswordBaru() {
        var x = document.getElementById("password-baru");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

    function showPasswordBaruKonfirmasi() {
        var x = document.getElementById("password-baru-konfirmasi");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>   