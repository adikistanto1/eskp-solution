        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>SKP Aktif</h3>
                            <p class="text-subtitle text-muted">
                               Merupakan SKP yang sedang berlaku, SKP yang akan dinilai</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">SKP Aktif</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tidak ada SKP Aktif</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Saat ini Anda tidak mempunyai SKP yang sedang aktif. Silahkan buat kemudian ajukan verifikasi ke pejabat penilai SKP Anda!
                                        </p>
                                        <a href="<?= base_url()?>pns/skp/baru" class="btn btn-primary rounded-pill mt-2">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Buat SKP
                                        </a> 
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                
                
            </div>
            <!-- <div class="buttons">
                 <a href="#" class="btn btn-danger rounded-pill"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;Batal</a>
                <a href="#" class="btn btn-warning rounded-pill"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan sebagai draft</a>
                <a href="<?= base_url()?>admin/skp/inputkegiatan" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Input Capaian Kinerja</a>
            </div> -->
        </div>
        
    