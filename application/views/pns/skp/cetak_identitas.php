<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<style>
th, td {
    padding: 2px;
}

#tabel_utama,#tabel_utama th,#tabel_utama td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 10px;
}
</style>
<div class="isi">
    <table width="100%" style="background-color: white;font-size: 10px">
         <tr>            
            <th width="50%">
                
            </th>
            <td width="50%">               
                
                Pekalongan, <?php echo date('d').' '. convert_month(date('m')).' '.date('Y');?><br>
                Kepada Yth.<br>
                Walikota Pekalongan<br>
                cq. Kepala Badan Kepegawaian Pendidikan<br>
                dan Pelatihan Daerah Kota Pekalongan<br>
                di<br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                PEKALONGAN
            </td>
        </tr>
    </table><br>
    <table width="100%" style="background-color: white;font-size: 10px">
         <tr>            
            <th width="100%">
                
         <center>
             <b><u>SURAT PENGANTAR</u></b><br>
             Nomor:....../......<br><br><br><br>
         </center>
            </th>
         </tr>
    </table>
  
   
    <table id="tabel_utama" width="100%" style="background-color: white;font-size: 9px;">
        <tr>
            <th width="5%" style="text-align:center">No</th>
            <th width="50%" style="text-align:center">Jenis Yang Dikirim</th>
            <th width="20%" style="text-align:center">Banyaknya</th>
            <th width="25%" style="text-align:center">Keterangan</th>
        </tr>
        <tr>
            <td style="text-align:center">1</td>
            <td>
              
            </td>
            <td style="text-align:center">
               
            </td>
            <td>
                Dikirim dengan hormat untuk menjadi periksa dan digunakan seperlunya
            </td>
        </tr>
    </table>
    
    
    
    <pagebreak></pagebreak>
    
    <table width="100%" style="background-color: white;font-size: 10px">
         <tr>            
            <th width="100%">
                
         <center>
             <b><u>LAMPIRAN FOTO</u></b><br>
             <br><br>
         </center>
            </th>
         </tr>
    </table>
  
   
    <table id="tabel_utama" width="100%" style="background-color: white;font-size: 9px;">
        <tr>
            <th width="5%" style="text-align:center">No</th>
            <th width="50%" style="text-align:center">Nama</th>
            <th width="20%" style="text-align:center">Kode</th>
            <th width="25%" style="text-align:center">Foto</th>
        </tr>
       
        <tr>
            
            
            <td style="text-align:center"></td>
            <td>           
               
            </td>
            <td style="text-align:center">
              
            </td>
            <td>
                Foto 3x4 hitam putih 3 lembar(masukkan platik,streples)
            </td>
        </tr>
        
    </table>
</div>

<?php
    function convert_month($kode){
        if($kode=='01'){
            return 'Januari';
        }else if($kode=='02'){
            return 'Februari';
        }else if($kode=='03'){
            return 'Maret';
        }else if($kode=='04'){
            return 'April';
        }else if($kode=='05'){
            return 'Mei';
        }else if($kode=='06'){
            return 'Juni';
        }else if($kode=='07'){
            return 'Juli';
        }else if($kode=='08'){
            return 'Agustus';
        }else if($kode=='09'){
            return 'September';
        }else if($kode==10){
            return 'Oktober';
        }else if($kode==11){
            return 'November';
        }else if($kode==12){
            return 'Desember';
        }
    }
        
    function convert_pangkat($kode){
        if($kode=='II/a'){
            return 'Pengatur Muda';
        }else if($kode=='II/b'){
            return 'Pengatur Muda Tingkat I';
        }else if($kode=='II/c'){
            return 'Pengatur';
        }else if($kode=='II/d'){
            return 'Pengatur Tingkat I';
        }else if($kode=='III/a'){
            return 'Penata Muda';
        }else if($kode=='III/b'){
            return 'Penata Muda Tingkat I';
        }else if($kode=='III/c'){
            return 'Penata';
        }else if($kode=='III/d'){
            return 'Penata Tingkat I';
        }else if($kode=='IV/a'){
            return 'Pembina';
        }else if($kode=='IV/b'){
            return 'Pembina Tingkat I';
        }else if($kode=='IV/c'){
            return 'Pembina Utama Muda';
        }else if($kode=='IV/d'){
            return 'Pembina Utama Madya';
        }else if($kode=='IV/e'){
            return 'Pembina Utama';
        }
    } ?>