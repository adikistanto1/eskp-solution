        

 <style>
 /* .dropdown-menu {
    background-color: #435ebe ;
    color:#fff;
    color: #333 ;
} */
 </style>
        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-8 order-md-1 order-last">
                            <h3>Detail Verifikasi Capaian SKP</h3>
                            <p class="text-subtitle text-muted">
                               Halaman ini berisi detail capaian SKP. <br>
                               Klik setuju untuk memverifikasi capaian SKP tersebut, klik revisi jika ada yang harus direvisi.</p>
                        </div>
                        <div class="col-12 col-md-4 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/skp/verifcapaian">Verifikasi SKP</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Capaian SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">    
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                            placeholder="Input Tanggal" required autocomplete="off" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_awal_skp']))?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input name="tgl_akhir" type="text" class="datepicker form-control square" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_akhir_skp']))?>"
                                            id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                     
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Capaian Kegiatan SKP &nbsp;&nbsp;
                                   
                                    <?php if($skp['status_skp_capaian']==1){
                                        echo '<span class="badge bg-primary">Menunggu verifikasi</span>';
                                    }?> 
                                    
                                </h4>                            
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table-capaian1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>Kategori</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th width="10%">Waktu</th>
                                            <th>Biaya</th>
                                            <th>Perhitungan</th>
                                            <th>Nilai</th>                                                                                    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        $total_nilai = 0;
                                        $kegiatan_dinilai = 0;
                                        foreach($kegiatan as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td rowspan="2"><?= $i;?></td>                                          
                                            <td rowspan="2"><?= $item['kegiatan_skp'];?></td>
                                            <td>Target</td>
                                            <td><?= $item['total_angka_kredit'];?></td>
                                            <td><?= $item['kuantitas_kegiatan']." ".$item['satuan_kecil'];?></td>
                                            <td><?= $item['mutu_kegiatan']." %";?></td>
                                            <td><?= $item['bulan_kegiatan']." Bulan";?></td>
                                            <td><?= "Rp ".$item['biaya_kegiatan'];?></td>                                            
                                            <?php
                                            $bulan_capaian = $item['bulan_capaian']=='' ? 0 : $item['bulan_capaian'];
                                            $biaya_capaian = $item['biaya_capaian']=='' ? 0 : $item['biaya_capaian'];
                                            $kuantitas_capaian = $item['kuantitas_capaian']=='' ? 0 : $item['kuantitas_capaian'];
                                            $kualitas_capaian = $item['mutu_capaian']=='' ? 0 : $item['mutu_capaian'];

                                            $persen_waktu = 100 - ($bulan_capaian/$item['bulan_kegiatan']*100);
                                            $persen_biaya = $item['biaya_kegiatan']==0 ? 0 : 100 - ($biaya_capaian/$item['biaya_kegiatan']*100);
                                            $kuantitas = $kuantitas_capaian/$item['kuantitas_kegiatan']*100;
                                            $kualitas = $kualitas_capaian/$item['mutu_kegiatan']*100;

                                            if($persen_waktu>24){
                                                $waktu = 76 - ((((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100)-100);
                                            }else{
                                                $waktu = ((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100;
                                            }

                                            if(($biaya_capaian==0)||($item['biaya_kegiatan']==0)){
                                                $biaya=0;
                                            }else{
                                                if($persen_biaya>24){
                                                    $biaya = 76 - ((((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100)-100);
                                                }else{
                                                    $biaya = ((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100;
                                                }
                                            }

                                            $perhitungan = $kuantitas + $kualitas + $waktu;
                                            $perhitungan1 = number_format((float)$perhitungan, 2, '.', '');
                                            $nilai       = $perhitungan1 / 3;
                                            $nilai1       = number_format((float)$nilai, 2, '.', '');

                                            if($item['kuantitas_kegiatan']>0){
                                                $kegiatan_dinilai = $kegiatan_dinilai + 1;
                                            }

                                            $total_nilai = $total_nilai + $nilai1;
                                            
                                            ?>

                                            <td rowspan="2"><?= $perhitungan1;?></td>   
                                            <td rowspan="2"><?= $nilai1;?></td>                                            
                                        </tr>
                                        <tr>
                                           
                                            <td>Realisasi</td>
                                            <td><?php if($item['total_angka_kredit_capaian']==''){echo '-';}else{echo $item['total_angka_kredit_capaian'];};?></td>
                                            <td><?php if($item['kuantitas_capaian']==''){echo '-';}else{echo $item['kuantitas_capaian']." ".$item['satuan_kecil'];};?></td>
                                            <td><?php if($item['mutu_capaian']==''){echo '-';}else{echo $item['mutu_capaian']." %";};?></td>
                                            <td><?php if($item['bulan_capaian']==''){echo '-';}else{echo $item['bulan_capaian']." Bulan";};?></td>
                                            <td><?php if($item['biaya_capaian']==''){echo '-';}else{echo "Rp ".$item['biaya_capaian'];};?></td> 
                                                                                    
                                        </tr>
                                        <?php endforeach;?>
                                        <tr>
                                            <?php
                                                $nilai_akhir = $total_nilai/$kegiatan_dinilai;
                                                if($nilai_akhir<=50){
                                                    $ket_nilai = "Buruk";
                                                }else if(($nilai_akhir>50)&&($nilai_akhir<=60)){
                                                    $ket_nilai = "Sedang";
                                                }else if(($nilai_akhir>60)&&($nilai_akhir<=75)){
                                                    $ket_nilai = "Sedang";
                                                }else if(($nilai_akhir>75)&&($nilai_akhir<=90.99)){
                                                    $ket_nilai = "Baik";
                                                }else if($nilai_akhir>90.99){
                                                    $ket_nilai = "Sangat Baik";
                                                }
                                            ?>
                                            <td colspan="9"><b>Nilai Capaian Kerja</b></td>                                            
                                            <td><b><?= number_format((float)$total_nilai/$kegiatan_dinilai, 2, '.', '').'<br> ('.$ket_nilai.')';?></b></td>                                             
                                            <td></td>                                    
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </div>              
            </div>
            <div class="buttons">
                <a href="<?php echo base_url(); ?>pns/skp/verifcapaian" class="btn btn-secondary rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali</a>                                
                <button id="delete-button" class="btn icon btn-danger rounded-pill"
                    data-bs-toggle="modal" 
                    
                    data-bs-target="#revisi"  
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" title="Revisi">
                        <i class="fa fa-marker" aria-hidden="true"></i>&nbsp;&nbsp;Revisi
                    </button>
                <button id="edit-button" class="btn icon btn-success rounded-pill"
                    data-bs-toggle="modal"                   
                    data-bs-target="#setuju"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" 
                    title="Setuju">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;Setuju
                </button>
            </div>          
        </div>


        <!--Danger theme Modal -->
    <div class="modal fade text-left" id="revisi" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <form action="<?php echo base_url(); ?>pns/skp/revisicapaian" method="post" id="form-hapus-draft">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Revisi Capaian SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">                                       
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                            <label for="squareText">Catatan Revisi</label>
                            <textarea type="text" class="form-control square" name="catatan-revisi" required
                                                    placeholder="Masukkan catatan revisi"></textarea>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-danger ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Kirim</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="setuju" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                
                <div class="modal-header bg-success">
                    <h5 class="modal-title white" id="myModalLabel120">
                        Konfirmasi Setuju Capaian SKP
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    Dengan klik setuju, berarti Anda menyetujui capaian SKP ini.Lanjutkan?                                             
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Batal</span>
                    </button>
                    <form action="<?php echo base_url(); ?>pns/skp/setujucapaian" method="post" id="form-edit-draft">
                        <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        <button type="submit" class="btn btn-success ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Setuju</span>
                        </button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
<script>
$(document).ready(function() {

    var nip_peg ='<?php echo $skp['nip_pegawai'];?>';
    var nip_pen ='<?php echo $skp['nip_penilai'];?>';
    var nip_at_pen ='<?php echo $skp['nip_atasan_penilai'];?>';
   
        
});





    
</script>      
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table-capaian');
    let dataTable = new simpleDatatables.DataTable(table1);     
</script> 
    