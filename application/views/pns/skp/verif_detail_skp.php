        

 <style>
 /* .dropdown-menu {
    background-color: #435ebe ;
    color:#fff;
    color: #333 ;
} */
 </style>
        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-8 order-md-1 order-last">
                            <h3>Detail Verifikasi SKP</h3>
                            <p class="text-subtitle text-muted">
                               Halaman ini berisi detail SKP. 
                               Meliputi periode, identias pegawai, pejabat penilai dan atasan pejabata penilai serta daftar kegiatan SKP</p>
                        </div>
                        <div class="col-12 col-md-4 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/skp/verifskp">Verifikasi SKP</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                            placeholder="Input Tanggal" required autocomplete="off" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_awal_skp']))?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input name="tgl_akhir" type="text" class="datepicker form-control square" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_akhir_skp']))?>"
                                            id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data identitas pegawai yang dinilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                                                                        
                                            <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                            value="<?=$skp['nip_pegawai']?>"
                                                placeholder="Input NIP" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-pegawai"
                                            value="<?=$skp['golongan_pegawai']?>" readonly
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-pegawai" readonly
                                                placeholder="Input Jabatan" required><?=$skp['jabatan_pegawai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai" readonly
                                                placeholder="Input OPD" required><?=$skp['opd_pegawai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data pejabat yang menilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                            
                                            <input type="text"  class="form-control square" name="nama-penilai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-penilai" readonly 
                                            value="<?=$skp['nip_penilai']?>"
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-penilai"
                                            value="<?=$skp['golongan_penilai']?>" readonly
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-penilai" readonly
                                                placeholder="Input Jabatan" required><?=$skp['jabatan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-penilai" readonly
                                                placeholder="Input OPD" required><?=$skp['opd_penilai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Atasan Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data atasan pejabat penilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>
                                            <input type="text"  class="form-control square" name="nama-atasan-penilai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-atasan-penilai" required readonly 
                                            value="<?=$skp['nip_atasan_penilai']?>"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-atasan-penilai" required readonly
                                            value="<?=$skp['golongan_atasan_penilai']?>"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-atasan-penilai" required readonly
                                                placeholder="Input Jabatan"><?=$skp['jabatan_atasan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-atasan-penilai" required readonly
                                                placeholder="Input OPD"><?=$skp['opd_atasan_penilai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Kegiatan SKP</h4>                            
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th>Waktu</th>
                                            <th>Biaya</th>                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($kegiatan as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>                                          
                                            <td><?= $item['kegiatan_skp'];?></td>
                                            <td><?= $item['total_angka_kredit'];?></td>
                                            <td><?= $item['kuantitas_kegiatan']." ".$item['satuan_kecil'];?></td>
                                            <td><?= $item['mutu_kegiatan']." %";?></td>
                                            <td><?= $item['bulan_kegiatan']." Bulan";?></td>
                                            <td><?= "Rp ".$item['biaya_kegiatan'];?></td>
                                            
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
            <div class="buttons">
                <a href="<?php echo base_url(); ?>pns/skp/verifskp" class="btn btn-secondary rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali</a>                                
                <button id="delete-button" class="btn icon btn-danger rounded-pill"
                    data-bs-toggle="modal" 
                    
                    data-bs-target="#revisi"  
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" title="Revisi">
                        <i class="fa fa-marker" aria-hidden="true"></i>&nbsp;&nbsp;Revisi
                    </button>
                <button id="edit-button" class="btn icon btn-success rounded-pill"
                    data-bs-toggle="modal"                   
                    data-bs-target="#setuju"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" 
                    title="Setuju">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;Setuju
                </button>
            </div>          
        </div>


        <!--Danger theme Modal -->
    <div class="modal fade text-left" id="revisi" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <form action="<?php echo base_url(); ?>pns/skp/revisiskp" method="post" id="form-hapus-draft">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Revisi SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">                                       
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                            <label for="squareText">Catatan Revisi</label>
                            <textarea type="text" class="form-control square" name="catatan-revisi" required
                                                    placeholder="Masukkan catatan revisi"></textarea>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-danger ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Kirim</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="setuju" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                
                <div class="modal-header bg-success">
                    <h5 class="modal-title white" id="myModalLabel120">
                        Konfirmasi Setuju SKP
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    Dengan menyetujui ini, berarti SKP ini akan aktif dan menjadi SKP yang dinilai.Lanjutkan?                                             
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Batal</span>
                    </button>
                    <form action="<?php echo base_url(); ?>pns/skp/setujuskp" method="post" id="form-edit-draft">
                        <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        <button type="submit" class="btn btn-success ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Setuju</span>
                        </button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
<script>
$(document).ready(function() {

    var nip_peg ='<?php echo $skp['nip_pegawai'];?>';
    var nip_pen ='<?php echo $skp['nip_penilai'];?>';
    var nip_at_pen ='<?php echo $skp['nip_atasan_penilai'];?>';
    cariNamaPegawai(nip_peg);
    cariNamaPenilai(nip_pen);
    cariNamaAtasanPenilai(nip_at_pen);
        
});



function cariNamaPegawai(nip_peg){
        var nama    = document.getElementsByName('nama-pegawai');      
       
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nip_peg},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }

    function cariNamaPenilai(nip_peg){
        var nama    = document.getElementsByName('nama-penilai');      
       
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nip_peg},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }

    function cariNamaAtasanPenilai(nip_peg){
        var nama    = document.getElementsByName('nama-atasan-penilai');      
       
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nip_peg},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }

    
</script>     
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);     
</script>   
    