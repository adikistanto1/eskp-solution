        

 <style>
 /* .dropdown-menu {
    background-color: #435ebe ;
    color:#fff;
    color: #333 ;
} */
 </style>
        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-8 order-md-1 order-last">
                            <h3>Detail Permintaan Nilai Perilaku</h3>
                            <p class="text-subtitle text-muted">
                               Halaman ini berisi form untuk menilai perilaku kerja SKP.</p>
                        </div>
                        <div class="col-12 col-md-4 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/skp/penilaian">Permintaan Nilai</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Penilaian</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">    
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                            placeholder="Input Tanggal" required autocomplete="off" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_awal_skp']))?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input name="tgl_akhir" type="text" class="datepicker form-control square" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_akhir_skp']))?>"
                                            id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                     
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Perilaku Kerja&nbsp;&nbsp;                                   

                                    <?php if($skp['status_skp_perilaku']==1){
                                        echo '<span class="badge bg-primary">Menunggu penilaian</span>';
                                    }?>
                            </div>
                            <form id="form-penilaian" action="<?php echo base_url(); ?>pns/skp/simpanpenilaian" method="post">       
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Orientasi Pelayanan *</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"   class="form-control square" value="0" name='pelayanan'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Integritas *</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"   class="form-control square" value="0" name='integritas'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Komitmen *</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"   class="form-control square" value="0" name='komitmen'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Disiplin *</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"   class="form-control square" value="0" name='disiplin'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kerjasama *</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"   class="form-control square" value="0" name='kerjasama'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kepemimpinan **</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="number" min="0" max="100"  class="form-control square" value="0" name='kepemimpinan'/>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <input type="hidden" name="id-skp" value="<?= $skp['id_skp'];?>"/>
                            
                            </form>                       
                        </div>
                    </div>
                </div> 
                <p>* Harus diisi<br> ** Diisi hanya untuk pegawai yang mempunyai bawahan</p>         
            </div>
            <div class="buttons">
                <a href="<?php echo base_url(); ?>pns/skp/penilaian" class="btn btn-secondary rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali</a>                                              
                <button id="edit-button" class="btn icon btn-success rounded-pill"
                    data-bs-toggle="modal"                   
                    data-bs-target="#setuju"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" 
                    title="Setuju">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;Setuju
                </button>
            </div>          
        </div>


        <!--Danger theme Modal -->    
    <div class="modal fade text-left" id="setuju" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel120"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                
                <div class="modal-header bg-success">
                    <h5 class="modal-title white" id="myModalLabel120">
                        Konfirmasi Penilaian
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    Dengan klik setuju, berarti Anda menyetujui penilaian ini.Lanjutkan?                                             
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Batal</span>
                    </button>
                    <button type="button" onclick="submitForm()" class="btn btn-success ml-1" data-bs-dismiss="modal">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Setuju</span>
                    </button>
                </div>
                
            </div>
        </div>
    </div>
<script>
$(document).ready(function() {

    var nip_peg ='<?php echo $skp['nip_pegawai'];?>';
    var nip_pen ='<?php echo $skp['nip_penilai'];?>';
    var nip_at_pen ='<?php echo $skp['nip_atasan_penilai'];?>';
   
        
});

function submitForm() {       

    var pelayanan     = document.getElementsByName('pelayanan');
    var integritas    = document.getElementsByName('integritas');
    var komitmen      = document.getElementsByName('komitmen');
    var disiplin      = document.getElementsByName('disiplin');      
    var kerjasama     = document.getElementsByName('kerjasama');
    var kepemimpinan  = document.getElementsByName('kepemimpinan');


    var isError = false;

    if((pelayanan[0].value==='')||(pelayanan[0].value==='0')){
        showErorValidate('Nilai orientasi pelayanan harus diisi!');
        var isError = true;
        return;
    }

    if((integritas[0].value==='')||(integritas[0].value==='0')){
        showErorValidate('Nilai integritas harus diisi!');
        var isError = true;
        return;
    }

    if((komitmen[0].value==='')||(komitmen[0].value==='0')){
        showErorValidate('Nilai komitmen harus diisi!');
        var isError = true;
        return;
    }

    if((disiplin[0].value==='')||(disiplin[0].value==='0')){
        showErorValidate('Nilai disiplin harus diisi!');
        var isError = true;
        return;
    }

    if((kerjasama[0].value==='')||(kerjasama[0].value==='0')){
        showErorValidate('Nilai kerjasama harus diisi!');
        var isError = true;
        return;
    }

    if(kepemimpinan[0].value===''){
        showErorValidate('Nilai kepemimpinan harus diisi!');
        var isError = true;
        return;
    }

    if(isError===false){
        document.getElementById("form-penilaian").submit();
        return;
    }
}

function showErorValidate(input){
    Swal.fire({
            icon: "error",
            title: "Oops...",
            text: input
        });
}
    
</script>      

    