        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>SKP Aktif</h3>
                            <p class="text-subtitle text-muted">
                               Merupakan SKP yang sedang berlaku, SKP yang akan dinilai</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">SKP Aktif</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePeriode" aria-expanded="true" aria-controls="collapseOne">
                                        Periode SKP
                                    </button>
                                    </h2>
                                    <div id="collapsePeriode" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="row">                                    
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="roundText">Tanggal Mulai</label>
                                                        <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                                        placeholder="Input Tanggal" required autocomplete="off" readonly
                                                        value="<?= date( "d-m-Y", strtotime($skp['tgl_awal_skp']))?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="squareText">Tanggal Selesai</label>
                                                        <input name="tgl_akhir" type="text" class="datepicker form-control square" readonly
                                                        value="<?= date( "d-m-Y", strtotime($skp['tgl_akhir_skp']))?>"
                                                        id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                
                            </div>
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePegawai" aria-expanded="true" aria-controls="collapseOne">
                                        Identitas Pegawai
                                    </button>
                                    </h2>
                                    <div id="collapsePegawai" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="row">                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="roundText">Nama</label>                                                                                        
                                                        <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                            placeholder="Input Nama" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="squareText">NIP</label>
                                                        <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                                        value="<?=$skp['nip_pegawai']?>"
                                                            placeholder="Input NIP" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="squareText">Pangkat, Golongan Ruang</label>
                                                        <input type="text"  class="form-control square" name="pangkat-pegawai"
                                                        value="<?=$skp['golongan_pegawai']?>" readonly
                                                            placeholder="Input pangkat, golongan ruang" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="squareText">Jabatan</label>
                                                        <textarea type="text"  class="form-control square" name="jabatan-pegawai" readonly
                                                            placeholder="Input Jabatan" required><?=$skp['jabatan_pegawai']?></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="squareText">OPD</label>
                                                        <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai" readonly
                                                            placeholder="Input OPD" required><?=$skp['opd_pegawai']?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                
                            </div>
                        </div>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePegawai" aria-expanded="true" aria-controls="collapseOne">
                                        Identitas Pegawai
                                    </button>
                                    </h2>
                                    <div id="collapsePegawai" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="row">                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="roundText">Nama</label>                                                                                        
                                                        <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                            placeholder="Input Nama" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="squareText">NIP</label>
                                                        <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                                        value="<?=$skp['nip_pegawai']?>"
                                                            placeholder="Input NIP" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="squareText">Pangkat, Golongan Ruang</label>
                                                        <input type="text"  class="form-control square" name="pangkat-pegawai"
                                                        value="<?=$skp['golongan_pegawai']?>" readonly
                                                            placeholder="Input pangkat, golongan ruang" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="squareText">Jabatan</label>
                                                        <textarea type="text"  class="form-control square" name="jabatan-pegawai" readonly
                                                            placeholder="Input Jabatan" required><?=$skp['jabatan_pegawai']?></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="squareText">OPD</label>
                                                        <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai" readonly
                                                            placeholder="Input OPD" required><?=$skp['opd_pegawai']?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                
                            </div>
                        </div>
                    </div>                
                </div>
                           

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                                                                        
                                            <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                            value="<?=$skp['nip_pegawai']?>"
                                                placeholder="Input NIP" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-pegawai"
                                            value="<?=$skp['golongan_pegawai']?>" readonly
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-pegawai" readonly
                                                placeholder="Input Jabatan" required><?=$skp['jabatan_pegawai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai" readonly
                                                placeholder="Input OPD" required><?=$skp['opd_pegawai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">                                   
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                            
                                            <input type="text"  class="form-control square" name="nama-penilai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-penilai" readonly 
                                            value="<?=$skp['nip_penilai']?>"
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-penilai"
                                            value="<?=$skp['golongan_penilai']?>" readonly
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-penilai" readonly
                                                placeholder="Input Jabatan" required><?=$skp['jabatan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-penilai" readonly
                                                placeholder="Input OPD" required><?=$skp['opd_penilai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Atasan Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>
                                            <input type="text"  class="form-control square" name="nama-atasan-penilai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-atasan-penilai" required readonly 
                                            value="<?=$skp['nip_atasan_penilai']?>"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-atasan-penilai" required readonly
                                            value="<?=$skp['golongan_atasan_penilai']?>"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-atasan-penilai" required readonly
                                                placeholder="Input Jabatan"><?=$skp['jabatan_atasan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-atasan-penilai" required readonly
                                                placeholder="Input OPD"><?=$skp['opd_atasan_penilai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Capaian Kegiatan SKP &nbsp;&nbsp;
                                    <?php foreach($kegiatan as $item){
                                          $id_capaian = $item['id_skp_capaian'];
                                    }?>
                                    <?php if($id_capaian==''){
                                        echo '<span class="badge bg-warning">Belum diisi</span>';
                                    }else{
                                        echo '<span class="badge bg-success">Sudah diisi</span>';
                                    }?>

                                    <?php if($skp['status_skp_capaian']==1){
                                        echo '<span class="badge bg-primary">Menunggu verifikasi</span>';
                                    }else if($skp['status_skp_capaian']==2){
                                        echo '<span class="badge bg-danger">Revisi</span><br>Catatan: '.$skp['revisi_skp_capaian'].'';
                                    }else if($skp['status_skp_capaian']==3){
                                        echo '<span class="badge bg-success">Disetujui</span>';
                                    }?>
                                    
                                </h4>                            
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table-capaian">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>Kategori</th>
                                            <th>AK</th>
                                            <th>Kuantias Output</th>
                                            <th>Kualitas Mutu</th>
                                            <th width="10%">Waktu</th>
                                            <th>Biaya</th>
                                            <th>Perhitungan</th>
                                            <th>Nilai</th> 
                                            <th>Aksi</th>                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        $total_nilai = 0;
                                        $kegiatan_dinilai = 0;
                                        foreach($kegiatan as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td rowspan="2"><?= $i;?></td>                                          
                                            <td rowspan="2"><?= $item['kegiatan_skp'];?></td>
                                            <td>Target</td>
                                            <td><?= $item['total_angka_kredit'];?></td>
                                            <td><?= $item['kuantitas_kegiatan']." ".$item['output_kegiatan'];?></td>
                                            <td><?= $item['mutu_kegiatan']." %";?></td>
                                            <td><?= $item['bulan_kegiatan']." Bulan";?></td>
                                            <td><?= "Rp ".$item['biaya_kegiatan'];?></td>                                            
                                            <?php
                                            $bulan_capaian = $item['bulan_capaian']=='' ? 0 : $item['bulan_capaian'];
                                            $biaya_capaian = $item['biaya_capaian']=='' ? 0 : $item['biaya_capaian'];
                                            $kuantitas_capaian = $item['kuantitas_capaian']=='' ? 0 : $item['kuantitas_capaian'];
                                            $kualitas_capaian = $item['mutu_capaian']=='' ? 0 : $item['mutu_capaian'];

                                            $persen_waktu = 100 - ($bulan_capaian/$item['bulan_kegiatan']*100);
                                            $persen_biaya = $item['biaya_kegiatan']==0 ? 0 : 100 - ($biaya_capaian/$item['biaya_kegiatan']*100);
                                            $kuantitas = $kuantitas_capaian/$item['kuantitas_kegiatan']*100;
                                            $kualitas = $kualitas_capaian/$item['mutu_kegiatan']*100;

                                            if($persen_waktu>24){
                                                $waktu = 76 - ((((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100)-100);
                                            }else{
                                                $waktu = ((1.76*$item['bulan_kegiatan']-$item['bulan_capaian'])/$item['bulan_kegiatan'])*100;
                                            }

                                            if(($biaya_capaian==0)||($item['biaya_kegiatan']==0)){
                                                $biaya=0;
                                            }else{
                                                if($persen_biaya>24){
                                                    $biaya = 76 - ((((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100)-100);
                                                }else{
                                                    $biaya = ((1.76*$item['biaya_kegiatan']-$item['biaya_capaian'])/$item['biaya_kegiatan'])*100;
                                                }
                                            }

                                            $perhitungan = $kuantitas + $kualitas + $waktu;
                                            $perhitungan1 = number_format((float)$perhitungan, 2, '.', '');
                                            $nilai       = $perhitungan1 / 3;
                                            $nilai1       = number_format((float)$nilai, 2, '.', '');

                                            if($item['kuantitas_kegiatan']>0){
                                                $kegiatan_dinilai = $kegiatan_dinilai + 1;
                                            }

                                            $total_nilai = $total_nilai + $nilai1;
                                            
                                            ?>

                                            <td rowspan="2"><?= $perhitungan1;?></td>   
                                            <td rowspan="2"><?= $nilai1;?></td>
                                            <td rowspan="2">
                                                
                                                <button id="edit-button" class="btn icon btn-success rounded-pill"
                                                    data-bs-toggle="modal" 
                                                    data-bs-target="#input-capaian"
                                                    data-id="<?=$item['id_kegiatan']?>"
                                                    data-id-capaian="<?=$item['id_skp_capaian']?>"                                                                                               
                                                    data-kegiatan="<?=$item['kegiatan_skp']?>"
                                                    data-kredit="<?=$item['total_angka_kredit']?>" 
                                                    data-kuantitas="<?=$item['kuantitas_kegiatan']?>"
                                                    data-output="<?=$item['output_kegiatan']?>"
                                                    data-mutu="<?=$item['mutu_kegiatan']?>"
                                                    data-bulan="<?=$item['bulan_kegiatan']?>"
                                                    data-biaya="<?=$item['biaya_kegiatan']?>"
                                                    data-kredit-capaian="<?=$item['total_angka_kredit_capaian']?>" 
                                                    data-kuantitas-capaian="<?=$item['kuantitas_capaian']?>"                                               
                                                    data-mutu-capaian="<?=$item['mutu_capaian']?>"
                                                    data-bulan-capaian="<?=$item['bulan_capaian']?>"
                                                    data-biaya-capaian="<?=$item['biaya_capaian']?>"
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top" title="Input Capaian"
                                                    <?php if(($skp['status_skp_capaian']==1)||($skp['status_skp_capaian']==3)){echo 'disabled';}?>>
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                           
                                            <td>Realisasi</td>
                                            <td><?php if($item['total_angka_kredit_capaian']==''){echo '-';}else{echo $item['total_angka_kredit_capaian'];};?></td>
                                            <td><?php if($item['kuantitas_capaian']==''){echo '-';}else{echo $item['kuantitas_capaian']." ".$item['output_kegiatan'];};?></td>
                                            <td><?php if($item['mutu_capaian']==''){echo '-';}else{echo $item['mutu_capaian']." %";};?></td>
                                            <td><?php if($item['bulan_capaian']==''){echo '-';}else{echo $item['bulan_capaian']." Bulan";};?></td>
                                            <td><?php if($item['biaya_capaian']==''){echo '-';}else{echo "Rp ".$item['biaya_capaian'];};?></td> 
                                                                                    
                                        </tr>
                                        <?php endforeach;?>
                                        <tr>
                                           
                                            <td colspan="9">Nilai Capaian Kerja</td>                                            
                                            <td><?= $total_nilai/$kegiatan_dinilai;?></td> 
                                            <td></td>                                    
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            <button class="btn btn-success rounded-pill"
                                data-bs-toggle="modal"                                
                                data-bs-target="#konfirm-capaian"
                                data-bs-toggle="tooltip"
                                data-bs-placement="top" 
                                title="Verifikasi Capaian"
                                <?php if(($skp['status_skp_capaian']==1)||($skp['status_skp_capaian']==3)){echo 'disabled';}?>>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Kirim verifikasi capaian
                            </button>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- =========================PERILAKU KERJA AWAL==========================================-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Perilaku Kerja&nbsp;&nbsp;
                                
                                    <?php if($skp['status_skp_perilaku']==1){
                                        echo '<span class="badge bg-primary">Menunggu penilaian</span>';
                                    }else if($skp['status_skp_perilaku']==2){
                                        echo '<span class="badge bg-success">Sudah dinilai</span>';
                                    }?>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Orientasi Pelayanan</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['pelayanan']?>" readonly/>
                                        </div>
                                    </div>
                               
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Integritas</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['integritas']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Komitmen</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['komitmen']?>" readonly/>
                                        </div>
                                    </div>
                               
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Disiplin</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['disiplin']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kerjasama</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['kerjasama']?>" readonly/>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kepemimpinan</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['kepemimpinan']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText"><b>Total Nilai</b></label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group"> 
                                            <?php
                                                $pel    = $skp['pelayanan'];
                                                $int    = $skp['integritas'];
                                                $kom    = $skp['komitmen'];
                                                $dis    = $skp['disiplin'];
                                                $ker    = $skp['kerjasama'];
                                                $kep    = $skp['kepemimpinan'];

                                                if($kep==0){
                                                    $total  = $pel + $int + $kom + $dis + $ker;
                                                    $rata   = $total / 5;
                                                }else{
                                                    $total  = $pel + $int + $kom + $dis + $ker + $kep;
                                                    $rata   = $total / 6;
                                                }
                                            
                                            ?>                                        
                                            <input type="text"  class="form-control square" value="<?=$total?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText"><b>Rata - rata</b></label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                                                         
                                            <input type="text"  class="form-control square" value="<?=$rata?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success rounded-pill"
                                    data-bs-toggle="modal"                                
                                    data-bs-target="#request-nilai"
                                    data-bs-toggle="tooltip"
                                    data-bs-placement="top" 
                                    title="Minta Nilai"
                                    <?php if(($skp['status_skp_perilaku']==1)||($skp['status_skp_perilaku']==2)){echo 'disabled';}?>>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Kirim permintaan nilai
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- =========================PERILAKU KERJA AKHIR==========================================-->
            </div>
            <div class="buttons">
                <a href="#" class="btn btn-secondary rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali</a>
                <button type="button" class="btn btn-danger rounded-pill"
                    data-bs-toggle="modal"                                
                    data-bs-target="#batalkan-skp"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" 
                    title="Batalkan SKP">
                    <i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;Batalkan SKP
                </button>
                <button type="button" class="btn btn-success rounded-pill"
                    data-bs-toggle="modal"                                
                    data-bs-target="#selesai-skp"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top" 
                    title="Selesai SKP"
                    <?php if(($skp['status_skp_capaian']!=3)&&($skp['status_skp_perilaku']!=2)){echo 'disabled';}?>>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Selesai
                </button>
            </div>
        </div>
        <?php       
            if (isset($success)) {
                echo    '<script type="text/javascript">
                Toastify({
                    text: "'. $success.'",
                    duration: 3000,
                    close:true,
                    gravity:"top",
                    position: "center",
                    backgroundColor: "#4fbe87",
                }).showToast();
                </script>';
            } 
        ?>
        <!--Modal input capaian awal-->
        <div class="modal fade text-left " id="input-capaian" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel160"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl"
                role="document">
                <div class="modal-content">
                <form id="form-edit-capaian" action="<?php echo base_url(); ?>pns/skp/simpancapaian" method="post">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">
                            Input Capaian Kegiatan SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id-skp-kegiatan" name="id-skp-kegiatan" value="0"/>
                        <input type="hidden" id="id-skp-capaian" name="id-skp-capaian" value="0"/>
                        <div class="row">                            
                            <div class="col-12">
                                <p>Input capaian kegiatan.
                                </p>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="roundText">Butir Kegiatan</label>
                                    <textarea type="text" id="kegiatan" class="form-control round" name="butir-kegiatan" readonly
                                        placeholder="Input butir kegiatan" ></textarea>
                                </div>
                            </div>
                            <h4>Target Kegiatan</h4>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="roundText">Total Angka Kredit</label>
                                    <input type="number"  class="form-control round" id="kredit" name="total-angka-kredit" readonly
                                        placeholder="Input Total AK" min="0" value="0.00" step=".01">                                   
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Kuantitas/Output</label>
                                <fieldset>
                                    <div class="input-group">                                                                      
                                        <input type="number" aria-label="First name" id="kuantitas" class="form-control" name="kuantitas-kegiatan" readonly
                                            placeholder="Kuantitas" min="0" value="0" step="1">
                                        <select class="form-control" id="output" name="output-kegiatan" disabled>
                                            <option value="0">Pilih</option>
                                            <option value="1">Kegiatan</option>
                                            <option value="2">Laporan</option>
                                            <option value="3">Dokumen</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Kualitas Mutu</label>
                               
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" id="mutu" placeholder="Input" name="mutu-kegiatan" readonly                                    
                                    min="0" value="100" max="100" step="1">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Waktu Realisasi</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" placeholder="Input" name="bulan-kegiatan" readonly
                                    min="0" value="0" step="1" max="12" id="bulan">
                                    <span class="input-group-text" id="basic-addon2">Bulan</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Biaya</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input type="dec" min="0" value="0" class="form-control" placeholder="Input" name="biaya-kegiatan" readonly
                                        aria-label="Username" aria-describedby="basic-addon1" id="biaya">
                                </div>
                            </div>
                            <h4>Capaian Kegiatan</h4>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="roundText">Total Angka Kredit **</label>
                                    <input type="number"  class="form-control round" id="capaian-kredit" name="capaian-total-angka-kredit"
                                        placeholder="Input Total AK" min="0" value="0.00" step=".01">                                   
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Kuantitas/Output *</label>
                                <fieldset>
                                    <div class="input-group">                                                                      
                                        <input type="number" aria-label="First name" id="capaian-kuantitas" class="form-control" name="capaian-kuantitas-kegiatan"
                                            placeholder="Kuantitas" min="0" value="0" step="1">
                                        <select class="form-control" id="output" name="output-kegiatan" disabled>
                                            <option value="0">Pilih</option>
                                            <option value="1">Kegiatan</option>
                                            <option value="2">Laporan</option>
                                            <option value="3">Dokumen</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Kualitas Mutu *</label>
                               
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" id="capaian-mutu" placeholder="Input" name="capaian-mutu-kegiatan"                                     
                                    min="0" value="100" max="100" step="1">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label for="roundText">Waktu Realisasi *</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" placeholder="Input" name="capaian-bulan-kegiatan"
                                    min="0" value="0" step="1" max="12" id="capaian-bulan">
                                    <span class="input-group-text" id="basic-addon2">Bulan</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="roundText">Biaya *</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input type="dec" min="0" value="0" class="form-control" placeholder="Input" name="capaian-biaya-kegiatan"
                                        aria-label="Username" aria-describedby="basic-addon1" id="capaian-biaya">
                                </div>
                            </div>
                            <p class="float-left">*Semua isian wajib diisi<br>**Bagi pejabat non fungsional total angka kredit diisi 0</p>
                        </div>                                              
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id-skp-pegawai" value="<?= $skp['id_skp'];?>"/>
                        
                        <button type="button"
                            class="btn btn-light-secondary rounded-pill"
                            data-bs-dismiss="modal">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Batal</span>
                        </button>
                        <button type="button" onclick="submitForm()"  class="btn btn-primary ml-1 rounded-pill">
                           
                            <span class="d-none d-sm-block"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan</span>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!--Modal input capaian akhir-->
        <!-- Modal konfirm verif capaian awal-->
        <div class="modal fade text-left" id="konfirm-capaian" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Konfirmasi Verifikasi Capaian
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="<?php echo base_url(); ?>pns/skp/kirimcapaian" method="post" id="form-edit-draft">
                    <div class="modal-body">
                        Yakin mengirim verifikasi capaian?
                        
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Tidak</span>
                        </button>
                        <button type="submit" class="btn btn-success ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Iya</span>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
         <!-- Modal konfirm verif capaian akhir-->
         <!-- Modal permintaan nilai awal-->
        <div class="modal fade text-left" id="request-nilai" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Konfirmasi Permintaan Nilai
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="<?php echo base_url(); ?>pns/skp/kirimperilaku" method="post">
                    <div class="modal-body">
                        Yakin mengirim permintaan nilai perilaku kerja?
                        
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Tidak</span>
                        </button>
                        <button type="submit" class="btn btn-success ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Iya</span>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
         <!-- Modal permintaan akhir-->
        <!-- Modal selesai SKP awal-->
        <div class="modal fade text-left" id="selesai-skp" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Konfirmasi Selesai SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="<?php echo base_url(); ?>pns/skp/selesaiskp" method="post">
                    <div class="modal-body">
                        Anda yakin SKP ini sudah selesai?
                        
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Tidak</span>
                        </button>
                        <button type="submit" class="btn btn-success ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Iya</span>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
         <!-- Modal selesai SKP akhir-->
         <!-- Modal batalkan SKP awal-->
        <div class="modal fade text-left" id="batalkan-skp" tabindex="-1"
            role="dialog" aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title white" id="myModalLabel120">
                            Konfirmasi Batalkan SKP
                        </h5>
                        <button type="button" class="close"
                            data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="<?php echo base_url(); ?>pns/skp/batalkanskp" method="post">
                    <div class="modal-body">
                        Batalkan SKP berarti SKP ini sudah tidak dapat lagi digunakan dan Anda harus membuat SKP baru. Lanjutkan?
                        
                            <input type="hidden" name="id-skp" id="id-skp" value="<?=$skp['id_skp']?>"/>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-light-secondary"
                            data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Tidak</span>
                        </button>
                        <button type="submit" class="btn btn-danger ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Lanjutkan</span>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
         <!-- Modal batalkan SKP akhir-->
<script>
    $(document).ready(function() {

    var nip_peg ='<?php echo $skp['nip_pegawai'];?>';
    var nip_pen ='<?php echo $skp['nip_penilai'];?>';
    var nip_at_pen ='<?php echo $skp['nip_atasan_penilai'];?>';
    cariNamaPegawai(nip_peg);
    cariNamaPenilai(nip_pen);
    cariNamaAtasanPenilai(nip_at_pen);
        
    });



    function cariNamaPegawai(nip_peg){
        var nama    = document.getElementsByName('nama-pegawai');      
    
        $.ajax({
        url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
        method: 'post',
        data:{nip:nip_peg},
        dataType: 'json',
        success: function(response){
        var len = response.length;

        if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
        }else{
            alert("Data tidak ditemukan");
        }

        }
    });
    }

    function cariNamaPenilai(nip_peg){
        var nama    = document.getElementsByName('nama-penilai');      
    
        $.ajax({
        url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
        method: 'post',
        data:{nip:nip_peg},
        dataType: 'json',
        success: function(response){
        var len = response.length;

        if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
        }else{
            alert("Data tidak ditemukan");
        }

        }
    });
    }

    function cariNamaAtasanPenilai(nip_peg){
        var nama    = document.getElementsByName('nama-atasan-penilai');      
    
        $.ajax({
        url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
        method: 'post',
        data:{nip:nip_peg},
        dataType: 'json',
        success: function(response){
        var len = response.length;

        if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
        }else{
            alert("Data tidak ditemukan");
        }

        }
    });
    }

    $(document).on("click", "#edit-button", function () {
        var id          = $(this).data('id');
        var id_capaian  = 0;       
        if(!$(this).data('id-capaian')==''){
            id_capaian = $(this).data('id-capaian');
        };
        var kegiatan    = $(this).data('kegiatan');
        var kredit      = $(this).data('kredit');
        var kuantitas   = $(this).data('kuantitas');
        var output      = $(this).data('output');
        var mutu        = $(this).data('mutu');
        var bulan       = $(this).data('bulan');
        var biaya       = $(this).data('biaya');
        var capaian_kredit      = $(this).data('kredit-capaian');
        var capaian_kuantitas   = $(this).data('kuantitas-capaian');       
        var capaian_mutu        = $(this).data('mutu-capaian');
        var capaian_bulan       = $(this).data('bulan-capaian');
        var capaian_biaya       = $(this).data('biaya-capaian');
        $(".modal-body #id-skp-kegiatan").val( id );
        $(".modal-body #id-skp-capaian").val( id_capaian );
        $(".modal-body #kegiatan").val( kegiatan );
        $(".modal-body #kredit").val( kredit );
        $(".modal-body #kuantitas").val( kuantitas );
        $(".modal-body #output").val( output );
        $(".modal-body #mutu").val( mutu );
        $(".modal-body #bulan").val( bulan );
        $(".modal-body #biaya").val( biaya );
        $(".modal-body #capaian-kredit").val( capaian_kredit );
        $(".modal-body #capaian-kuantitas").val( capaian_kuantitas );    
        $(".modal-body #capaian-mutu").val( capaian_mutu );
        $(".modal-body #capaian-bulan").val( capaian_bulan );
        $(".modal-body #capaian-biaya").val( capaian_biaya );
    }); 

function submitForm() {       

    var angka_kredit    = document.getElementsByName('capaian-total-angka-kredit');
    var kuantitas       = document.getElementsByName('capaian-kuantitas-kegiatan');       
    var mutu            = document.getElementsByName('capaian-mutu-kegiatan');
    var bulan           = document.getElementsByName('capaian-bulan-kegiatan');
    var biaya           = document.getElementsByName('capaian-biaya-kegiatan'); 

    var isError = false;

  

    if(angka_kredit[0].value===''){
        showErorValidate('Capaian total angka kredit harus diisi!');
        var isError = true;
        return;
    }

    if(kuantitas[0].value===''){
        showErorValidate('Capaian kuantitas kegiatan harus diisi!');
        var isError = true;
        return;
    }    

    if(mutu[0].value===''){
        showErorValidate('Capaian mutu kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(bulan[0].value===''){
        showErorValidate('Capaian bulan kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(biaya[0].value===''){
        showErorValidate('Capaian biaya kegiatan harus diisi!');
        var isError = true;
        return;
    }

    if(isError===false){
        document.getElementById("form-edit-capaian").submit();
        return;
    }
}

function showErorValidate(input){
    Swal.fire({
            icon: "error",
            title: "Oops...",
            text: input
        });
}
</script>
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table-capaian');
    let dataTable = new simpleDatatables.DataTable(table1);     
</script>
        
    