        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Riwayat SKP</h3>
                            <p class="text-subtitle text-muted">                               
                               Berisi daftar SKP yang sudah disetujui pejabat penilai</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Riwayat SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">                           
                            <div class="card-body">
                                <table class="table table-striped" id="table1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Periode SKP</th>
                                            <th>Tahun</th>
                                            <th>Tgl Buat</th>
                                            <th>Status SKP</th>
                                            <th width="5%">Aksi</th>
                                                                                      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 0;
                                        foreach($riwayat as $item) :
                                        $i = $i + 1;
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>                                          
                                            <td><?= date( "d M Y", strtotime($item['tgl_awal_skp']))." - ".date( "d M Y", strtotime($item['tgl_akhir_skp']));?></td>
                                            <td><?= date( "Y", strtotime($item['tgl_awal_skp']))?></td>
                                            <td><?= date( "d M Y H:i:s", strtotime($item['create_date_skp']))?></td>
                                            <td><?php
                                                 if($item['status_skp_pegawai']==1){
                                                    echo '<span class="badge bg-primary">menunggu verifikasi</span>';
                                                 }else if($item['status_skp_pegawai']==3){
                                                    echo '<span class="badge bg-success">aktif</span>';
                                                 }else if($item['status_skp_pegawai']==2){
                                                    echo '<span class="badge bg-danger">revisi</span><br>Catatan:<br>'.$item['revisi_skp_pegawai'].'';
                                                 }else if($item['status_skp_pegawai']==4){
                                                    echo '<span class="badge bg-secondary">selesai</span>';
                                                 }else if($item['status_skp_pegawai']==5){
                                                    echo '<span class="badge bg-secondary">dibatalkan</span>';
                                                 }
                                                 
                                                 ?></td>
                                            <td>
                                            <form action="<?php echo base_url(); ?>pns/skp/lihatskp" method="post" id="form-lihat-skp">
                                                <input type="hidden" name="id-skp" id="id-skp" value="<?= $item['id_skp'];?>"/>                   
                                            
                                                <button type="submit"  id="edit-button" class="btn icon btn-info rounded-pill"                                                
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top" title="Lihat">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                            </td>                                           
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                           
        </div>       
    </div>
    <?php
        $message = $this->session->flashdata('message');
        if (isset($message)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'.$message.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        }
        
        if (isset($success)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'.$success.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        }
    ?>
    
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable = new simpleDatatables.DataTable(table1);  
    

    function submitLihatForm() {       
        document.getElementById("form-lihat-skp").submit();
    }
</script>
        
    