        

        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Verifikasi Capaian SKP</h3>
                            <p class="text-subtitle text-muted">                               
                               Berisi daftar capaian SKP yang harus Anda verifikasi</p>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Verifikasi Capaian SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">                        
                        <div class="card">   
                            <div class="card-header">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#pending"
                                            role="tab" aria-controls="home" aria-selected="true">
                                            Pending Verifikasi Capaian SKP
                                            <?php 
                                                $i = 0;
                                                foreach($skp as $item){
                                                    if($item['status_skp_capaian']==1){
                                                        $i = $i + 1;
                                                    }
                                                }                                                
                                            ?>
                                            &nbsp;&nbsp;<span class="badge bg-dark"><?= $i;?></span>
                                            </a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#riwayat"
                                            role="tab" aria-controls="profile" aria-selected="false">
                                            Riwayat Verifikasi Capaian SKP
                                            <?php 
                                                $i = 0;
                                                foreach($skp as $item){
                                                    if($item['status_skp_capaian']>1){
                                                        $i = $i + 1;
                                                    }
                                                }                                                
                                            ?>
                                            &nbsp;&nbsp;<span class="badge bg-dark"><?= $i;?></span>
                                            </a>
                                    </li>                                    
                                </ul>
                            </div>                        
                            <div class="card-body">                                
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="pending" role="tabpanel"
                                            aria-labelledby="home-tab">
                                        <table class="table table-striped" id="table1">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>PNS</th>
                                                    <th>Periode SKP</th>
                                                    <!-- <th>Tahun</th> -->
                                                    <th>Tgl Buat</th>
                                                    <th>Status Capaian</th>
                                                    <th width="5%">Aksi</th>
                                                                                            
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $i = 0;
                                                foreach($skp as $item) :
                                                if($item['status_skp_capaian']==1):
                                                $i = $i + 1;
                                                ?>
                                                <tr>
                                                    <td><?= $i;?></td>
                                                    <td><?= $item['gelar_depan']." ".$item['nama_pegawai'].", ".$item['gelar_belakang'];?></td>                                             
                                                    <td><?= date( "d M Y", strtotime($item['tgl_awal_skp']))." - ".date( "d M Y", strtotime($item['tgl_akhir_skp']));?></td>
                                                    <!-- <td><?= date( "Y", strtotime($item['tgl_awal_skp']))?></td> -->
                                                    <td><?= date( "d M Y H:i:s", strtotime($item['create_date_skp']))?></td>
                                                    <td><span class="badge bg-primary">menunggu verifikasi</span></td>
                                                    <td>
                                                    <form action="<?php echo base_url(); ?>pns/skp/verifcapaiandetail" method="post" id="form-lihat-skp">
                                                        <input type="hidden" name="id-skp" id="id-skp" value="<?= $item['id_skp'];?>"/>                   
                                                    
                                                        <button type="submit"  id="edit-button" class="btn icon btn-info rounded-pill"                                                
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="top" title="Lihat">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </button>

                                                    </form>
                                                    </td>                                           
                                                </tr>
                                                <?php endif;endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="riwayat" role="tabpanel"
                                            aria-labelledby="home-tab">
                                        <table class="table table-striped" id="table2">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>PNS</th>
                                                    <th>Periode SKP</th>
                                                    <!-- <th>Tahun</th> -->
                                                    <th>Tgl Buat</th>
                                                    <th>Status Capaian</th>
                                                    <th width="5%">Aksi</th>
                                                                                            
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $i = 0;
                                                foreach($skp as $item) :
                                                if($item['status_skp_capaian']>1):
                                                $i = $i + 1;
                                                ?>
                                                <tr>
                                                    <td><?= $i;?></td>
                                                    <td><?= $item['gelar_depan']." ".$item['nama_pegawai'].", ".$item['gelar_belakang'];?></td>                                             
                                                    <td><?= date( "d M Y", strtotime($item['tgl_awal_skp']))." - ".date( "d M Y", strtotime($item['tgl_akhir_skp']));?></td>
                                                    <!-- <td><?= date( "Y", strtotime($item['tgl_awal_skp']))?></td> -->
                                                    <td><?= date( "d M Y H:i:s", strtotime($item['create_date_skp']))?></td>
                                                    <td><?php
                                                        if($item['status_skp_capaian']==3){
                                                            echo '<span class="badge bg-success">disetujui</span>';
                                                        }else if($item['status_skp_capaian']==2){
                                                            echo '<span class="badge bg-danger">revisi</span><br>Catatan:<br>'.$item['revisi_skp_capaian'].'';
                                                        }else{
                                                            echo '<span class="badge bg-primary">menunggu verifikasi</span>';
                                                        }
                                                        
                                                        ?></td>
                                                    <td>
                                                    <form action="<?php echo base_url(); ?>pns/skp/riwayatverifdetailcapaian" method="post" id="form-lihat-skp">
                                                        <input type="hidden" name="id-skp" id="id-skp" value="<?= $item['id_skp'];?>"/>                   
                                                    
                                                        <button type="submit" id="edit-button" class="btn icon btn-info rounded-pill"                                                
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="top" title="Lihat">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </button>

                                                        </form>
                                                    </td>                                           
                                                </tr>
                                                <?php endif;endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
        </div>       
    </div>
    <?php
        $message = $this->session->flashdata('message');
        if (isset($message)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'.$message.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        }
        
        if (isset($success)) {
            echo    '<script type="text/javascript">
            Toastify({
                text: "'.$success.'",
                duration: 3000,
                close:true,
                gravity:"top",
                position: "center",
                backgroundColor: "#4fbe87",
            }).showToast();
            </script>';
        }
    ?>
    
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table1');
    let dataTable1 = new simpleDatatables.DataTable(table1);  

    let table2 = document.querySelector('#table2');
    let dataTable2 = new simpleDatatables.DataTable(table2);  
    

    function submitLihatForm() {       
        document.getElementById("form-lihat-skp").submit();
    }
</script>
        
    