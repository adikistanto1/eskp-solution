        

 <style>
 /* .dropdown-menu {
    background-color: #435ebe ;
    color:#fff;
    color: #333 ;
} */
 </style>
        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-8 order-md-1 order-last">
                            <h3>Buat SKP Baru (1 dari 2 halaman)</h3>
                            <p class="text-subtitle text-muted">
                               Halaman ini adalah untuk membuat SKP baru. 
                               Pada halaman ini berisi pengisian periode SKP, pejabat penilai dan atasan pejabata penilai.</p>
                        </div>
                        <div class="col-12 col-md-4 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/dashboard/index">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Buat SKP</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">
                <form id="form-edit-draft" method="post" action="<?php echo base_url(); ?>pns/skp/simpanedit">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                            placeholder="Input Tanggal" required autocomplete="off" 
                                            value="<?= date( "d-m-Y", strtotime($draft['tgl_awal_skp']))?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input name="tgl_akhir" type="text" class="datepicker form-control square"
                                            value="<?= date( "d-m-Y", strtotime($draft['tgl_akhir_skp']))?>"
                                            id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data identitas pegawai yang dinilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama*</label>                                                                                        
                                            <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP*</label>
                                            <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                            value="<?=$draft['nip_pegawai']?>"
                                                placeholder="Input NIP" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang*</label>
                                            <input type="text"  class="form-control square" name="pangkat-pegawai"
                                            value="<?=$draft['golongan_pegawai']?>"
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan*</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-pegawai"
                                                placeholder="Input Jabatan" required><?=$draft['jabatan_pegawai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD*</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai"
                                                placeholder="Input OPD" required><?=$draft['opd_pegawai']?></textarea>
                                        </div>
                                    </div>
                                    <p>*Jika GOLONGAN, JABATAN dan OPD tidak sesuai saat pengisian otomatis, silahkan edit secara manual. Kemudian laporkan ke BKPPD agar data tersebut diperbarui.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data pejabat yang menilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama*</label>
                                            <select onchange="cariDataPenilai()" name="nama-penilai" class="choices form-select" required>
                                                <option value="">Pilih</option>
                                                <?php foreach($nama_nip as $item):?>
                                                <option value="<?php echo $item['nip_baru']?>" 
                                                <?=$item['nip_baru'] == $draft['nip_penilai'] ? ' selected="selected"' : '';?>><?php echo $item['gelar_depan']." ".$item['nama_pegawai']." ".$item['gelar_belakang']?></option>                                                                              
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP*</label>
                                            <input type="text"  class="form-control square" name="nip-penilai" readonly 
                                            value="<?=$draft['nip_penilai']?>"
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang*</label>
                                            <input type="text"  class="form-control square" name="pangkat-penilai"
                                            value="<?=$draft['golongan_penilai']?>"
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan*</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-penilai"
                                                placeholder="Input Jabatan" required><?=$draft['jabatan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD*</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-penilai"
                                                placeholder="Input OPD" required><?=$draft['opd_penilai']?></textarea>
                                        </div>
                                    </div>
                                    <p>*Jika GOLONGAN, JABATAN dan OPD tidak sesuai saat pengisian otomatis, silahkan edit secara manual. Kemudian laporkan ke BKPPD agar data tersebut diperbarui.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Atasan Pejabat Penilai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data atasan pejabat penilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>
                                            <select onchange="cariDataAtasanPenilai()" name="nama-atasan-penilai" class="choices form-select" required>
                                                <option value="">Pilih</option>
                                                <?php foreach($nama_nip as $item):?>
                                                <option value="<?php echo $item['nip_baru']?>" 
                                                <?=$item['nip_baru'] == $draft['nip_atasan_penilai'] ? ' selected="selected"' : '';?>><?php echo $item['gelar_depan']." ".$item['nama_pegawai']." ".$item['gelar_belakang']?></option>                                                                              
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-atasan-penilai" required readonly 
                                            value="<?=$draft['nip_atasan_penilai']?>"
                                                placeholder="Input Nama">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang*</label>
                                            <input type="text"  class="form-control square" name="pangkat-atasan-penilai" required
                                            value="<?=$draft['golongan_atasan_penilai']?>"
                                                placeholder="Input pangkat, golongan ruang">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan*</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-atasan-penilai" required
                                                placeholder="Input Jabatan"><?=$draft['jabatan_atasan_penilai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD*</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-atasan-penilai" required
                                                placeholder="Input OPD"><?=$draft['opd_atasan_penilai']?></textarea>
                                        </div>
                                    </div>
                                    <p>*Jika GOLONGAN, JABATAN dan OPD tidak sesuai saat pengisian otomatis, silahkan edit secara manual. Kemudian laporkan ke BKPPD agar data tersebut diperbarui.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>* Semua isian wajib diisi!</p>
            </div>
            <div class="buttons">
                <a href="<?php echo base_url(); ?>pns/skp/draft" class="btn btn-danger rounded-pill"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;Batal</a>
                <input id="is-draft-flag" type="hidden" name="is-draft" value="0"/>
                <input  type="hidden" name="id-skp" value="<?= $draft['id_skp']?>"/>
                <?php if($draft['status_skp_pegawai']!=2):?>
                <button type="button" onclick="submitForm(1)" class="btn btn-warning rounded-pill">
                    <i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Simpan sebagai draft
                </button>
                <?php endif;?>
                <button type="button" onclick="submitForm(0)" class="btn btn-success rounded-pill"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;Selanjutnya isi kegiatan SKP</button>
            </div>
            </form>
        </div>
<script>
$(document).ready(function() {

    var nip_peg ='<?php echo $nip;?>';
    cariDataPegawai(nip_peg);

     $('.datepicker').datepicker({
            format: "dd-mm-yyyy", 
            todayHighlight: true,                 
            autoclose: true
        });

        
});

function submitForm(code) {    
    var awal     = document.getElementsByName('tgl_awal');
    var akhir     = document.getElementsByName('tgl_akhir');

    var nip     = document.getElementsByName('nip-pegawai');
    var nama    = document.getElementsByName('nama-pegawai');      
    var pangkat = document.getElementsByName('pangkat-pegawai');
    var jabatan = document.getElementsByName('jabatan-pegawai');
    var opd     = document.getElementsByName('opd-pegawai'); 

    var nipp     = document.getElementsByName('nip-penilai');
    var namap    = document.getElementsByName('nama-penilai');      
    var pangkatp = document.getElementsByName('pangkat-penilai');
    var jabatanp = document.getElementsByName('jabatan-penilai');
    var opdp     = document.getElementsByName('opd-penilai');   

    var nipap     = document.getElementsByName('nip-atasan-penilai');
    var namaap    = document.getElementsByName('nama-atasan-penilai');      
    var pangkatap = document.getElementsByName('pangkat-atasan-penilai');
    var jabatanap = document.getElementsByName('jabatan-atasan-penilai');
    var opdap     = document.getElementsByName('opd-atasan-penilai');

    var isError = false;
  
    if(awal[0].value===''){
        showErorValidate('Tanggal awal ');
        var isError = true;
        return;
    }

    if(akhir[0].value===''){
        showErorValidate('Tanggal akhir ');
        var isError = true;
        return;
    }

    if(nama[0].value===''){
        showErorValidate('Nama pegawai ');
        var isError = true;
        return;
    }

    if(nip[0].value===''){
        showErorValidate('NIP pegawai ');
        var isError = true;
        return;
    }

    if(pangkat[0].value===''){
        showErorValidate('Pangkat pegawai ');
        var isError = true;
        return;
    }

    if(jabatan[0].value===''){
        showErorValidate('Jabatan Pegawai ');
        var isError = true;
        return;
    }

    if(opd[0].value===''){
        showErorValidate('OPD pegawai ');
        var isError = true;
        return;
    }

    if(namap[0].value===''){
        showErorValidate('Nama pejabat penilai ');
        var isError = true;
        return;
    }

    if(nipp[0].value===''){
        showErorValidate('NIP pejabat penilai ');
        var isError = true;
        return;
    }

    if(pangkatp[0].value===''){
        showErorValidate('Pangkat pejabat penilai ');
        var isError = true;
        return;
    }

    if(jabatanp[0].value===''){
        showErorValidate('Jabatan pejabat penilai ');
        var isError = true;
        return;
    }

    if(opdp[0].value===''){
        showErorValidate('OPD pejabat penilai ');
        var isError = true;
        return;
    }

    if(namaap[0].value===''){
        showErorValidate('Nama atasan pejabat penilai ');
        var isError = true;
        return;
    }

    if(nipap[0].value===''){
        showErorValidate('NIP atasan pejabat penilai ');
        var isError = true;
        return;
    }

    if(pangkatap[0].value===''){
        showErorValidate('Pangkat atasan pejabat penilai ');
        var isError = true;
        return;
    }

    if(jabatanap[0].value===''){
        showErorValidate('Jabatan atasan pejabat penilai ');
        var isError = true;
        return;
    }

    if(opdap[0].value===''){
        showErorValidate('OPD atasan pejabat penilai ');
        var isError = true;
        return;
    }

    //alert(nip[0].value)

    document.getElementById("is-draft-flag").value=code;
    document.getElementById("form-edit-draft").submit();
}

function showErorValidate(input){
    Swal.fire({
            icon: "error",
            title: "Oops...",
            text: input+" harus diisi!"
        });
}

function cariDataPegawai(nip_peg){
        var nip     = document.getElementsByName('nip-pegawai');
        var nama    = document.getElementsByName('nama-pegawai');      
        var pangkat = document.getElementsByName('pangkat-pegawai');
        var jabatan = document.getElementsByName('jabatan-pegawai');
        var opd     = document.getElementsByName('opd-pegawai');        
        
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nip_peg},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                             
                nip[0].value=response[0]['nip_baru']; 
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                            
                //pangkat[0].value=response[0]['golongan_ruang'];
                //jabatan[0].value=response[0]['nama_jabatan'];
                //opd[0].value=response[0]['opd'];
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }

    function cariDataPenilai(){
        var nip     = document.getElementsByName('nip-penilai');
        var nama    = document.getElementsByName('nama-penilai');      
        var pangkat = document.getElementsByName('pangkat-penilai');
        var jabatan = document.getElementsByName('jabatan-penilai');
        var opd     = document.getElementsByName('opd-penilai');        
        
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nama[0].value},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                             
                nip[0].value=response[0]['nip_baru'];                                             
                pangkat[0].value=response[0]['golongan_ruang'];
                jabatan[0].value=response[0]['nama_jabatan'];
                opd[0].value=response[0]['opd'];
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }

    function cariDataAtasanPenilai(){
        var nip     = document.getElementsByName('nip-atasan-penilai');
        var nama    = document.getElementsByName('nama-atasan-penilai');      
        var pangkat = document.getElementsByName('pangkat-atasan-penilai');
        var jabatan = document.getElementsByName('jabatan-atasan-penilai');
        var opd     = document.getElementsByName('opd-atasan-penilai');        
        
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nama[0].value},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                             
                nip[0].value=response[0]['nip_baru'];                                             
                pangkat[0].value=response[0]['golongan_ruang'];
                jabatan[0].value=response[0]['nama_jabatan'];
                opd[0].value=response[0]['opd'];
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }
</script>       
    