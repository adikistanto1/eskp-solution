        

 <style>
 /* .dropdown-menu {
    background-color: #435ebe ;
    color:#fff;
    color: #333 ;
} */
 </style>
        <div id="main-content">        
            <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-8 order-md-1 order-last">
                            <h3>Detail Penilaian Perilaku</h3>
                            <p class="text-subtitle text-muted">
                               Halaman ini berisi detail penilaian perilaku kerja.
                              </p>
                        </div>
                        <div class="col-12 col-md-4 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url()?>pns/skp/penilaian">Penilaian Perilaku</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Riwayat Penilaian</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            <div class="page-content">   
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Periode SKP</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Atur periode SKP dari tanggal mulai hingga tanggal selesai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Tanggal Mulai</label>
                                            <input name="tgl_awal" type="text" id="tgl_awal" class="datepicker form-control round" 
                                            placeholder="Input Tanggal" required autocomplete="off" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_awal_skp']))?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="squareText">Tanggal Selesai</label>
                                            <input name="tgl_akhir" type="text" class="datepicker form-control square" readonly
                                            value="<?= date( "d-m-Y", strtotime($skp['tgl_akhir_skp']))?>"
                                            id="tgl_akhir" placeholder="Input Tanggal" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Identitas Pegawai</h4>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p>Masukkan data identitas pegawai yang dinilai.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="roundText">Nama</label>                                                                                        
                                            <input type="text"  class="form-control square" name="nama-pegawai" readonly
                                                placeholder="Input Nama" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">NIP</label>
                                            <input type="text"  class="form-control square" name="nip-pegawai" readonly 
                                            value="<?=$skp['nip_pegawai']?>"
                                                placeholder="Input NIP" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="squareText">Pangkat, Golongan Ruang</label>
                                            <input type="text"  class="form-control square" name="pangkat-pegawai"
                                            value="<?=$skp['golongan_pegawai']?>" readonly
                                                placeholder="Input pangkat, golongan ruang" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">Jabatan</label>
                                            <textarea type="text"  class="form-control square" name="jabatan-pegawai" readonly
                                                placeholder="Input Jabatan" required><?=$skp['jabatan_pegawai']?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="squareText">OPD</label>
                                            <textarea type="text" id="squareText" class="form-control square" name="opd-pegawai" readonly
                                                placeholder="Input OPD" required><?=$skp['opd_pegawai']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Perilaku Kerja&nbsp;&nbsp;
                                
                                    <?php if($skp['status_skp_perilaku']==2){
                                        echo '<span class="badge bg-success">Sudah dinilai</span>';
                                    }?>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Orientasi Pelayanan</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['pelayanan']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Integritas</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['integritas']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Komitmen</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['komitmen']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Disiplin</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['disiplin']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kerjasama</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['kerjasama']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText">Kepemimpinan</label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                         
                                            <input type="text"  class="form-control square" value="<?=$skp['kepemimpinan']?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText"><b>Total Nilai</b></label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group"> 
                                            <?php
                                                $pel    = $skp['pelayanan'];
                                                $int    = $skp['integritas'];
                                                $kom    = $skp['komitmen'];
                                                $dis    = $skp['disiplin'];
                                                $ker    = $skp['kerjasama'];
                                                $kep    = $skp['kepemimpinan'];

                                                if($kep==0){
                                                    $total  = $pel + $int + $kom + $dis + $ker;
                                                    $rata   = $total / 5;
                                                }else{
                                                    $total  = $pel + $int + $kom + $dis + $ker + $kep;
                                                    $rata   = $total / 6;
                                                }
                                            
                                            ?>                                        
                                            <input type="text"  class="form-control square" value="<?=$total?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="roundText"><b>Rata - rata</b></label>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                                                                         
                                            <input type="text"  class="form-control square" value="<?=$rata?>" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
                            
            </div>
            <div class="buttons">
                <a href="<?php echo base_url(); ?>pns/skp/penilaian" class="btn btn-secondary rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali</a>                                                
            </div>          
        </div>

<script>
$(document).ready(function() {

    var nip_peg ='<?php echo $skp['nip_pegawai'];?>';
    var nip_pen ='<?php echo $skp['nip_penilai'];?>';
    var nip_at_pen ='<?php echo $skp['nip_atasan_penilai'];?>';
   
    cariNamaPegawai(nip_peg);
        
});


function cariNamaPegawai(nip_peg){
        var nama    = document.getElementsByName('nama-pegawai');      
       
        $.ajax({
         url:'<?=base_url()?>pns/skp/ajaxgetdatapegawai',
         method: 'post',
         data:{nip:nip_peg},
         dataType: 'json',
         success: function(response){
           var len = response.length;

           if(len > 0){                                            
                nama[0].value=response[0]['gelar_depan']+" "+response[0]['nama_pegawai']+" " +response[0]['gelar_belakang'];                                                           
           }else{
             alert("Data tidak ditemukan");
           }

         }
       });
    }


    
</script>      
<script>
    // Simple Datatable
    let table1 = document.querySelector('#table-capaian');
    let dataTable = new simpleDatatables.DataTable(table1);     
</script> 
    