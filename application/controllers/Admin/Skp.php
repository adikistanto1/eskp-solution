<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skp extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="Admin BKPPD") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_Pegawai');
        $this->load->model('M_Skp');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-aktif";
        
         
        $this->load->viewAdminSkp('admin/skp/aktif_skp',$data);
    }

    public function baru()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['menu'] = "skp-baru";

        $data['nama_nip'] = $this->M_Pegawai->get_pegawai_all();
         
        $this->load->viewAdminSkp('admin/skp/buat_skp',$data);
    }

    public function simpanbaru(){
                

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-baru";

        
        $is_draft = $this->input->post('is-draft');
        if($is_draft==0){
            $id_skp_pegawai  = $this->M_Skp->simpan_baru_draft();
            $data['id_skp_pegawai'] = $id_skp_pegawai;
            $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
            $this->load->viewAdminSkp('admin/skp/buat_skp_kegiatan',$data);
        }else{
            $data['menu'] = "skp-draft";
            $id_skp_pegawai  = $this->M_Skp->simpan_baru_draft();
            $data['draft'] = $this->M_Skp->get_draft_all();
            $this->load->viewAdminSkp('admin/skp/draft_skp',$data);
        }
        

    }

   
    public function inputkegiatan()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-baru";
        
         
        $this->load->viewAdminSkp('admin/skp/buat_skp_kegiatan',$data);
    }

    public function simpankegiatan(){
        
        
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-baru";

        $this->M_Skp->simpan_kegiatan();

        $id_skp_pegawai  = $this->input->post('id-skp-pegawai');      
        $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
        $data['id_skp_pegawai'] = $id_skp_pegawai;

        $id_skp_kegiatan = $this->input->post('id-skp-kegiatan');
        if($id_skp_kegiatan==0){
            $data['success'] = 'Berhasil menambah kegiatan... ';
        }else{
            $data['success'] = 'Berhasil mengubah kegiatan... ';
        }
        

        $this->load->viewAdminSkp('admin/skp/buat_skp_kegiatan',$data);
    }

    public function hapuskegiatan() {

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-baru";

        $id = $this->input->post('id-skp-kegiatan');
        $this->M_Skp->hapus_kegiatan($id);
        
        $id_skp_pegawai  = $this->input->post('id-skp-pegawai'); 
        $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
        $data['id_skp_pegawai'] = $id_skp_pegawai;

        $data['success'] = 'Berhasil menghapus kegiatan... ';

        $this->load->viewAdminSkp('admin/skp/buat_skp_kegiatan',$data);
    }

     /* SUB MENU  DRAFT*/
    public function draft()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-draft";

        $nip = $this->session->userdata('skp_username');
        
        $data['draft'] = $this->M_Skp->get_draft_all();
         
        $this->load->viewAdminSkp('admin/skp/draft_skp',$data);
    }

    public function hapusdraft() {      

        $id = $this->input->post('id-skp');
        $this->M_Skp->hapus_draft($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('admin/skp/draft');
    }

    public function editdraft()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['menu'] = "skp-baru";

        $data['nama_nip'] = $this->M_Pegawai->get_pegawai_all();

        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['draft']      = $this->M_Skp->get_draft_by_id($id_skp_pegawai);
         
        $this->load->viewAdminSkp('admin/skp/edit_draft',$data);
    }

    public function simpanedit(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-baru";

        
        $is_draft       = $this->input->post('is-draft');        
        $id_skp_pegawai = $this->input->post('id-skp');

        if($is_draft==0){
            $this->M_Skp->simpan_edit_draft();
            $data['id_skp_pegawai'] = $id_skp_pegawai;
            $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
            $this->load->viewAdminSkp('admin/skp/buat_skp_kegiatan',$data);
        }else{
            $data['menu'] = "skp-draft";
            $this->M_Skp->simpan_edit_draft();
            $data['draft'] = $this->M_Skp->get_draft_all();
            $this->load->viewAdminSkp('admin/skp/draft_skp',$data);
        }
        

    }

    public function aktif()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-aktif";
        
         
        $this->load->viewAdminSkp('admin/skp/aktif_skp',$data);
    }
    
    public function riwayat()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['menu'] = "skp-riwayat";
        
         
        $this->load->viewAdminSkp('admin/skp/riwayat_skp',$data);
    }


    /* ======================================================= AJAX =========================================== */
    public function ajaxgetdatapegawai()
    {
        $nip     = $this->input->post('nip');
        $data    = $this->M_Pegawai->get_pegawai_by_nip($nip);
        echo json_encode($data);
    }
     
}
