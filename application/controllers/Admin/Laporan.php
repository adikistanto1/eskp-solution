<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="Admin BKPPD") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
       
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        
         
        $this->load->viewAdminLaporan('admin/laporan/laporan',$data);
    }
    
     
}
