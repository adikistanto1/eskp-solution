<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('M_Auth');
		$this->load->model('M_Skp');
	}
	
	public function index()
	{	
            if($this->session->userdata('skp_logged_in')==false){
                $this->load->view('login');
            }else{
                $role = $this->session->userdata('skp_role_code');
                $this->setRedirect($role);

            }
	}

	public function loginform(){
		$this->load->view('login');
	}
        
	public function login()
	{
		//set_rules validasi
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|sha1');
		
		if ($this->form_validation->run() === FALSE)//jika gagal/tidak memenuhi aturan
		{
                    $this->load->view('login');	
		}
		else //jika berhasil/memenuhi aturan
		{
			$row = $this->M_Auth->check_user($this->input->post('username'),$this->input->post('password'));//cek apakah user terdaftar
			//echo $this->input->post('username').' '.$this->input->post('password');return;
			if(!empty($row)){	//jika terdaftar
				$data_user = $this->M_Auth->get_user($this->input->post('username'));
				$status_verifikator = $this->M_Auth->cek_status_verifikator($this->input->post('username'));
                                
				$sess_array = array(
				'skp_id_user'   		=> $data_user['id_admin_skp'],
				'skp_username'          => $data_user['username_admin_skp'],
				'skp_name'              => $data_user['nama_admin_skp'],
				'skp_satuan_kerja'      => $data_user['id_opd'],
				'skp_role'              => $this->getRoleName($data_user['level_admin_skp']),
				'skp_role_code'         => $data_user['level_admin_skp'],
				'skp_verifikator'       => $status_verifikator,
				'skp_logged_in' => TRUE
				);
				
				$this->session->set_userdata($sess_array);		
                $this->setRedirect($data_user['level_admin_skp']);
                                
			}else{
				$data['error'] = 'Gagal Login, username atau password tidak terdaftar';
				$this->load->view('login',$data);
			}
		}
		
	}
	
        private function getRoleName($role){
            if($role == 'admin'){
                return "Admin BKPPD";
            }else if($role == 'pns'){
                return "PNS";
            }
        }
        
        private function setRedirect($role){
            if($role == 'admin'){
                return redirect('admin/dashboard/index');
            }else if($role == 'pns'){
                return redirect('pns/dashboard/index');
            }
        }

        public function logout(){
            if($this->session->userdata('skp_logged_in')==false){
                    $this->load->view('login');
            }
            $this->session->sess_destroy();
            //$this->load->view('login');
            return redirect('auth');
	}
	
}

/* End of file c_auth.php */
/* Location: ./application/controllers/admin/c_auth.php */