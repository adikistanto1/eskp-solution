<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skp extends CI_Controller {

    /*  KODE STATUS SKP
        0 = draft
        1 = menunggu verifikasi skp
        2 = revisi skp
        3 = setujui aktif
        4 = selesai
        5 = in aktif

        KODE STATUS CAPAIAN
        1 = kirim verif
        2 = revisi capaian
        3 = setuju capaian

        KODE STATUS PERILAKU
        1 = kirim verif
        2 = dinilai
       
       

       
    
    */
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="PNS") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_Pegawai');
        $this->load->model('M_Skp');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-aktif";
        
         
        $this->load->viewPNSSkp('pns/skp/aktif_skp',$data);
    }

    public function baru()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $data['nama_nip'] = $this->M_Pegawai->get_pegawai_all();
         
        $this->load->viewPNSSkp('pns/skp/buat_skp',$data);
    }

    public function simpanbaru(){
                

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $nip = $this->session->userdata('skp_username');

        $is_draft = $this->input->post('is-draft');
        if($is_draft==0){
            $id_skp_pegawai  = $this->M_Skp->simpan_baru_draft();
            $data['id_skp_pegawai'] = $id_skp_pegawai;
            $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);

            $data['satuan'] = $this->M_Skp->get_satuan_kegiatan_kecil();

            $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
        }else{
            $data['menu'] = "skp-draft";
            $id_skp_pegawai  = $this->M_Skp->simpan_baru_draft();
            $data['draft'] = $this->M_Skp->get_draft_all_by_nip($nip);
            $this->load->viewPNSSkp('pns/skp/draft_skp',$data);
        }

    }

   
    public function inputkegiatan()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $data['satuan'] = $this->M_Skp->get_satuan_kegiatan_kecil();
         
        $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
    }

    public function simpankegiatan(){
        
        
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $this->M_Skp->simpan_kegiatan();

        $id_skp_pegawai  = $this->input->post('id-skp-pegawai');      
        $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
        $data['id_skp_pegawai'] = $id_skp_pegawai;

        $id_skp_kegiatan = $this->input->post('id-skp-kegiatan');
        if($id_skp_kegiatan==0){
            $data['success'] = 'Berhasil menambah kegiatan... ';
        }else{
            $data['success'] = 'Berhasil mengubah kegiatan... ';
        }
        
        $data['satuan'] = $this->M_Skp->get_satuan_kegiatan_kecil();

        $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
    }

    public function hapuskegiatan() {

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $id = $this->input->post('id-skp-kegiatan');
        $this->M_Skp->hapus_kegiatan($id);
        
        $id_skp_pegawai  = $this->input->post('id-skp-pegawai'); 
        $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
        $data['id_skp_pegawai'] = $id_skp_pegawai;

        $data['success'] = 'Berhasil menghapus kegiatan... ';

        $data['satuan'] = $this->M_Skp->get_satuan_kegiatan_kecil();

        $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
    }

    public function kirimverifikasi()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-riwayat";

        $id_skp_pegawai         = $this->input->post('id-skp-pegawai');

        $is_error = false;

        $terkirim   = $this->M_Skp->cek_skp_terkirim($this->session->userdata('skp_username'));
        $revisi     = $this->M_Skp->cek_skp_revisi($this->session->userdata('skp_username'),$id_skp_pegawai);
        $aktif      = $this->M_Skp->cek_skp_aktif($this->session->userdata('skp_username'));
        
        if($terkirim>0){
            $error_m    = 'Saat ini Anda sudah memiliki SKP yang sedang diverifikasi';
            $is_error   = true;
        }

        if($revisi>0){
            $error_m    = 'Saat ini Anda memiliki SKP yang berstatus revisi';
            $is_error   = true;
        }

        if($aktif>0){
            $error_m    = 'Saat ini Anda memiliki SKP yang berstatus aktif';
            $is_error   = true;
        }

        if($is_error){
            
            $data['kegiatan']       = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
            $data['id_skp_pegawai'] = $id_skp_pegawai;

            $data['error'] = $error_m;

            $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
        }else{
            $id_skp_pegawai     = $this->input->post('id-skp-pegawai');      
            $this->M_Skp->kirim_verifikasi_skp($id_skp_pegawai);
            $data['success'] = 'Berhasil mengirim SKP... ';

            $data['riwayat'] = $this->M_Skp->get_riwayat_all_by_nip($this->session->userdata('skp_username'));
            
            $this->load->viewPNSSkp('pns/skp/riwayat_skp',$data);
        }
        
    }

     /* ================================ SUB MENU  DRAFT ==========================================*/
    public function draft()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-draft";

        $nip = $this->session->userdata('skp_username');
        
        $data['draft'] = $this->M_Skp->get_draft_all_by_nip($nip);
         
        $this->load->viewPNSSkp('pns/skp/draft_skp',$data);
    }

    public function hapusdraft() {      

        $id = $this->input->post('id-skp');
        $this->M_Skp->hapus_draft($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('pns/skp/draft');
    }

    public function editdraft()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $data['nama_nip'] = $this->M_Pegawai->get_pegawai_all();

        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['draft']      = $this->M_Skp->get_draft_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/edit_draft',$data);
    }

    public function editrevisi()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $data['nama_nip'] = $this->M_Pegawai->get_pegawai_all();

        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['draft']      = $this->M_Skp->get_revisi_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/edit_draft',$data);
    }

    public function simpanedit(){                

        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-baru";

        $nip = $this->session->userdata('skp_username');
        
        $is_draft       = $this->input->post('is-draft');        
        $id_skp_pegawai = $this->input->post('id-skp');

        if($is_draft==0){
            $this->M_Skp->simpan_edit_draft();
            $data['id_skp_pegawai'] = $id_skp_pegawai;
            $data['kegiatan'] = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);

            $data['satuan'] = $this->M_Skp->get_satuan_kegiatan_kecil();
            
            $this->load->viewPNSSkp('pns/skp/buat_skp_kegiatan',$data);
        }else{
            $data['menu'] = "skp-draft";
            $this->M_Skp->simpan_edit_draft();
            $data['draft'] = $this->M_Skp->get_draft_all_by_nip($nip);
            $this->load->viewPNSSkp('pns/skp/draft_skp',$data);
        }
        

    }

    /* ================================== SKP AKTIF ==================================== */

    public function aktif()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-aktif";

        
        $status   = $this->M_Skp->cek_skp_aktif($this->session->userdata('skp_username'));
        if($status>0){
            $skp                = $this->M_Skp->get_skp_aktif($this->session->userdata('skp_username'));
            $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($skp['id_skp']);
            $data['skp']        = $skp;
            $this->load->viewPNSSkp('pns/skp/aktif_skp',$data);
        }else{
            $this->load->viewPNSSkp('pns/skp/aktif_skp_kosong',$data);
        }
    
    }

    public function simpancapaian(){
        
        
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-aktif";

        $this->M_Skp->simpan_capaian();

        $id_skp_capaian = $this->input->post('id-skp-capaian');
        if($id_skp_capaian==0){
            $data['success'] = 'Berhasil input capaian... ';
        }else{
            $data['success'] = 'Berhasil mengubah capaian... ';
        }
        
        $data['skp']        = $this->M_Skp->get_skp_aktif($this->session->userdata('skp_username'));
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($this->input->post('id-skp-pegawai'));
        
        $this->load->viewPNSSkp('pns/skp/aktif_skp',$data);
    
    }

    public function kirimcapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-aktif";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->kirim_capaian_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil mengirim capaian... ';

        $data['skp']        = $this->M_Skp->get_skp_aktif($this->session->userdata('skp_username'));
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($this->input->post('id-skp'));
         
        $this->load->viewPNSSkp('pns/skp/aktif_skp',$data);
    }

    public function kirimperilaku()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-aktif";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->kirim_perilaku_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil mengirim permintaan nilai... ';

        $data['skp']        = $this->M_Skp->get_skp_aktif($this->session->userdata('skp_username'));
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($this->input->post('id-skp'));
         
        $this->load->viewPNSSkp('pns/skp/aktif_skp',$data);
    }

    public function batalkanskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-riwayat";
        
        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->batalkan_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil membatalkan SKP... ';
         
        $this->load->viewPNSSkp('pns/skp/riwayat_skp',$data);
    }

    public function selesaiskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-riwayat";
        
        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->selesai_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil menyimpan... ';

        $data['riwayat'] = $this->M_Skp->get_riwayat_all_by_nip($this->session->userdata('skp_username'));
         
        $this->load->viewPNSSkp('pns/skp/riwayat_skp',$data);
    }
    

    /* ======================================= RIWAYAT ======================================== */
    public function riwayat()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-riwayat";
        
        $data['riwayat'] = $this->M_Skp->get_riwayat_all_by_nip($this->session->userdata('skp_username'));
         
        $this->load->viewPNSSkp('pns/skp/riwayat_skp',$data);
    }


    public function lihatskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-riwayat";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_perilaku_by_id($id_skp_pegawai);
        //$data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($id_skp_pegawai);

        $this->load->viewPNSSkp('pns/skp/riwayat_detail_skp',$data);
    }

    /* ================================== VERIFIKASI SKP ==================================== */

    public function verifskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-verif";

        $data['skp']  = $this->M_Skp->get_list_skp_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_skp',$data);
    
    }

    public function verifdetail()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-verif";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_by_id($id_skp_pegawai);
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_skp',$data);
    }

    public function riwayatverifdetail()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-verif";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_by_id($id_skp_pegawai);
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_riwayat_skp',$data);
    }

    public function setujuskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-verif";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->setuju_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil menyetujui SKP... ';

        $data['skp']  = $this->M_Skp->get_list_skp_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_skp',$data);
    
    }

    public function revisiskp()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-verif";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->revisi_skp($id_skp_pegawai);
        $data['success'] = 'Berhasil mengubah status revisi SKP... ';

        $data['skp']  = $this->M_Skp->get_list_skp_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_skp',$data);
    
    }

    /* ====================================== VERIF CAPAIAN ================================ */

    public function verifcapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-capaian";

        $data['skp']  = $this->M_Skp->get_list_capaian_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_capaian',$data);
    
    }

    public function verifcapaiandetail()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-capaian";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_by_id($id_skp_pegawai);
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_capaian',$data);
    }

    public function setujucapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-capaian";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->setuju_capaian($id_skp_pegawai);
        $data['success'] = 'Berhasil menyetujui capaian... ';

        $data['skp']  = $this->M_Skp->get_list_capaian_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_capaian',$data);
    
    }

    public function revisicapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-capaian";

        $id_skp_pegawai     = $this->input->post('id-skp');      
        $this->M_Skp->revisi_capaian($id_skp_pegawai);
        $data['success'] = 'Berhasil mengubah status revisi capaian SKP... ';

        $data['skp']  = $this->M_Skp->get_list_capaian_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_capaian',$data);
    
    }

    public function riwayatverifdetailcapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-capaian";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_by_id($id_skp_pegawai);
        $data['kegiatan']   = $this->M_Skp->get_skp_kegiatan_capaian_by_id($id_skp_pegawai);
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_riwayat_capaian',$data);
    }

    /* ====================================== PENILAIAN ================================ */

    public function penilaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";

        $data['skp']  = $this->M_Skp->get_list_penilaian_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_penilaian',$data);
    
    }

    public function verifpenilaiandetail()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_by_id($id_skp_pegawai);       
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_penilaian',$data);
    }

    public function simpanpenilaian(){
        
        
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";

        $this->M_Skp->simpan_penilaian();
        $this->M_Skp->setuju_penilaian();
        $data['success'] = 'Berhasil menyimpan penilaian... ';

        $data['skp']  = $this->M_Skp->get_list_penilaian_to_verif($this->session->userdata('skp_username'));

        $this->load->viewPNSSkp('pns/skp/verif_list_penilaian',$data);
    }

    public function riwayatverifdetailpenilaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";
        
        $id_skp_pegawai     = $this->input->post('id-skp'); 
        $data['skp']        = $this->M_Skp->get_skp_penilaian_by_id($id_skp_pegawai);        
         
        $this->load->viewPNSSkp('pns/skp/verif_detail_riwayat_penilaian',$data);
    }


    // ====================================================== CETAK ==============================================
    public function cetakidentitas()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";
        
        
        $html = $this->load->view('pns/skp/cetak_identitas', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function cetakcapaian()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";
        
        
        $html = $this->load->view('pns/skp/cetak_capaian', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function cetaktarget()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "skp-penilaian";
        
        
        $html = $this->load->view('pns/skp/cetak_capaian', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }



    /* ======================================================= AJAX =========================================== */
    public function ajaxgetdatapegawai()
    {
        $nip     = $this->input->post('nip');
        $data    = $this->M_Pegawai->get_pegawai_by_nip($nip);
        echo json_encode($data);
    }
     
}
