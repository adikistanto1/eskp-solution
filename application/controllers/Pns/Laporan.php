<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="PNS") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_Skp');
       
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        
        $data['skp'] = array();
        $data['status'] = "";
        $data['tahun']  = "";
         
        $this->load->viewPNSLaporan('pns/laporan/laporan',$data);
    }


    public function cari()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        
        $data['skp'] = $this->M_Skp->cari_laporan_skp($this->session->userdata('skp_username'));

        $data['status'] = $this->input->post('status');
        $data['tahun']  = $this->input->post('tahun');
         
        $this->load->viewPNSLaporan('pns/laporan/laporan',$data);
    }
    
     
}
