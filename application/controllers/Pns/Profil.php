<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="PNS") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_Pegawai');
        $this->load->model('M_Auth');
       
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "profil-index";
        
        $data['pegawai'] = $this->M_Pegawai->get_pegawai($this->session->userdata('skp_username'));
         
        $this->load->viewPNSProfil('pns/profil/profil',$data);
    }

    public function password()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "profil-password";
        
        $this->load->viewPNSProfil('pns/profil/password',$data);
    }

    public function ubahpassword(){
        
        
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        $data['menu'] = "profil-password";

        if($this->input->post('password-baru')!=$this->input->post('password-baru-konfirmasi')){
            $data['error'] = 'Gagal, konfirmasi password baru tidak cocok';
        }else{
            $row = $this->M_Auth->check_user($this->input->post('nip'),sha1($this->input->post('password-lama')));
            if(!empty($row)){	//jika terdaftar

                $this->M_Pegawai->ubah_password($this->input->post('nip'));

                $data['success'] = 'Berhasil merubah password... ';                        
            }else{
                $data['error'] = 'Gagal, password lama tidak cocok';
              
            }
        }

        $this->load->viewPNSProfil('pns/profil/password',$data);
    }
    
     
}
