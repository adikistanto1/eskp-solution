<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('skp_logged_in')) {
            redirect('auth/index');
        }
        else{
           if ($this->session->userdata('skp_role')!="PNS") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_Skp');
       
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('skp_name');
        $data['role'] = $this->session->userdata('skp_role');
        $data['nip']  = $this->session->userdata('skp_username');
        $data['verifikator']  = $this->session->userdata('skp_verifikator');
        
        $data['skp'] = $this->M_Skp->get_skp_all_by_nip($this->session->userdata('skp_username'));
        $data['list'] = $this->M_Skp->get_selesai_by_nip($this->session->userdata('skp_username'));
        $data['penilai'] = $this->M_Skp->get_penilai_selesai_by_nip($this->session->userdata('skp_username'));

        $data['id_skps'] = $this->M_Skp->get_id_selesai_by_nip($this->session->userdata('skp_username'));
         
        $this->load->viewPNSDashboard('pns/dashboard/dashboard',$data);
    }
    
     
}
